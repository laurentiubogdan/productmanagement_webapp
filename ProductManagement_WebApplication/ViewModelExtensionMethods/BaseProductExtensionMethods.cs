﻿using Kendo.Mvc.UI.Fluent;
using ProductManagement.Api.Helpers;
using ProductManagement.Api.Requests.BaseProduct;
using ProductManagement.Api.Responses.BaseProduct;
using ProductManagement.Domain.Helper_Objects;
using ProductManagement_WebApplication.ViewModels;
using ProductManagement_WebApplication.ViewModels.BaseProduct;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ProductManagement_WebApplication.ViewModelExtensionMethods
{
    public static class BaseProductExtensionMethods
    {
        public static BaseProductGridModel ToBaseProductGridModel(this BaseProductResponse response)
        {
            return new BaseProductGridModel
            {
                Id = response.Id,
                Name = response.Name,
                Description = response.Description,
                Type = response.Type.ToDesignView(),
            };
        }

        public static BaseProductViewModel ToBaseProductViewModel(this BaseProductResponse response)
        {
            return new BaseProductViewModel
            {
                Id = response.Id,
                Name = response.Name,
                ProductCode = response.ProductCode,
                UniqueCode = response.Name.GenerateUniqueCode(),
                Description = response.Description,
                Funder = response.Funder,
                Type = response.Type,
                DocumentationType = response.DocumentationType,
                PrimaryLoanPurpose = response.PrimaryLoanPurpose,
                PrincipalInterestTerm = response.PrincipalInterestTerm,
                InterestOnlyTerm = response.InterestOnlyTerm,
                PrepaidInterestTerm = response.PrepaidInterestTerm,
                InterestCapitalisedTerm = response.InterestCapitalisedTerm,
                PrincipalInterestFeesTerm = response.PrincipalInterestFeesTerm,
                VariableRateTerm = response.VariableRateTerm,
                FixedRateTerm = response.FixedRateTerm,
                LoanAmount = response.LoanAmount,
                Lvr = response.Lvr,
                RepaymentMethods = response.RepaymentMethods,
                RepaymentFrequency = response.RepaymentFrequency,
                StatementCycle = response.StatementCycle,
                SecurityPriority = response.SecurityPriority,
                NumberOfSplits = response.NumberOfSplits,
                GenuineSavings = response.GenuineSavings,
                BaseInterestRate = response.BaseInterestRate,
                LoanPurposes = response.LoanPurposes
            };
        }

        public static BaseProductCreateRequest ToBaseProductCreateRequest(this CreateBaseProductViewModel viewModel)
        {
            return new BaseProductCreateRequest
            {
                Name = viewModel.Name,
                ProductCode = viewModel.ProductCode,
                Type = viewModel.Type,
                Funder = viewModel.Funder,
                Description = viewModel.Description
            };
        }

        public static BaseProductUpdateRequest ToBaseProductUpdateRequest(this BaseProductResponse response)
        {
            return new BaseProductUpdateRequest
            {
                Name = response.Name,
                ProductCode = response.ProductCode,
                Description = response.Description,
                Funder = (Enums.FunderEnum)Enum.Parse(typeof(Enums.FunderEnum), response.Funder),
                Type = (Enums.ProductTypeEnum)Enum.Parse(typeof(Enums.ProductTypeEnum), response.Type),
                DocumentationType = response.DocumentationType.ToArrayIfNull().Select(p => (Enums.DocumentationType)Enum.Parse(typeof(Enums.DocumentationType), p)),
                PrimaryLoanPurpose = response.PrimaryLoanPurpose.ToArrayIfNull().Select(p => (Enums.PrimaryLoanPurpose)Enum.Parse(typeof(Enums.PrimaryLoanPurpose), p)),
                PrincipalInterestTerm = response.PrincipalInterestTerm.ToArrayIfNull().ToTermRequest(),
                InterestOnlyTerm = response.InterestOnlyTerm.ToArrayIfNull().ToTermRequest(),
                PrepaidInterestTerm = response.PrepaidInterestTerm.ToArrayIfNull().ToTermRequest(),
                InterestCapitalisedTerm = response.InterestCapitalisedTerm.ToArrayIfNull().ToTermRequest(),
                PrincipalInterestFeesTerm = response.PrincipalInterestFeesTerm.ToArrayIfNull().ToTermRequest(),
                VariableRateTerm = response.VariableRateTerm.ToArrayIfNull().ToTermRequest(),
                FixedRateTerm = response.FixedRateTerm.ToArrayIfNull().ToTermRequest(),
                LoanAmount = response.LoanAmount.ToArrayIfNull(),
                Lvr = response.Lvr.ToArrayIfNull(),
                RepaymentMethods = response.RepaymentMethods.ToArrayIfNull().Select(p => (Enums.RepaymentMethodsEnum)Enum.Parse(typeof(Enums.RepaymentMethodsEnum), p)),
                RepaymentFrequency = response.RepaymentFrequency.ToArrayIfNull().Select(p => (Enums.RepaymentFrequencyEnum)Enum.Parse(typeof(Enums.RepaymentFrequencyEnum), p)),
                StatementCycle = response.StatementCycle.ToArrayIfNull().Select(p => (Enums.StatementCycleEnum)Enum.Parse(typeof(Enums.StatementCycleEnum), p)),
                SecurityPriority = response.SecurityPriority.ToArrayIfNull().Select(p => (Enums.SecurityPriorityEnum)Enum.Parse(typeof(Enums.SecurityPriorityEnum), p)),
                NumberOfSplits = response.NumberOfSplits,
                GenuineSavings = response.GenuineSavings,
                BaseInterestRate = response.BaseInterestRate,
                LoanPurposes = response.LoanPurposes.ToArrayIfNull(),
                Features = response.Features.ToArrayIfNull()
            };
        }

        public static BaseProductUpdateRequest ToBaseProductUpdateRequest(this BaseProductViewModel viewModel)
        {
            return new BaseProductUpdateRequest
            {
                Name = viewModel.Name,
                ProductCode = viewModel.ProductCode,
                Description = viewModel.Description,
                Funder = (Enums.FunderEnum)Enum.Parse(typeof(Enums.FunderEnum), viewModel.Funder),
                Type = (Enums.ProductTypeEnum)Enum.Parse(typeof(Enums.ProductTypeEnum), viewModel.Type),
                DocumentationType = viewModel.DocumentationType.ToArrayIfNull().Select(p => (Enums.DocumentationType)Enum.Parse(typeof(Enums.DocumentationType), p)),
                PrimaryLoanPurpose = viewModel.PrimaryLoanPurpose.ToArrayIfNull().Select(p => (Enums.PrimaryLoanPurpose)Enum.Parse(typeof(Enums.PrimaryLoanPurpose), p)),
                PrincipalInterestTerm = viewModel.PrincipalInterestTerm.ToArrayIfNull().ToTermRequest(),
                InterestOnlyTerm = viewModel.InterestOnlyTerm.ToArrayIfNull().ToTermRequest(),
                PrepaidInterestTerm = viewModel.PrepaidInterestTerm.ToArrayIfNull().ToTermRequest(),
                InterestCapitalisedTerm = viewModel.InterestCapitalisedTerm.ToArrayIfNull().ToTermRequest(),
                PrincipalInterestFeesTerm = viewModel.PrincipalInterestFeesTerm.ToArrayIfNull().ToTermRequest(),
                VariableRateTerm = viewModel.VariableRateTerm.ToArrayIfNull().ToTermRequest(),
                FixedRateTerm = viewModel.FixedRateTerm.ToArrayIfNull().ToTermRequest(),
                LoanAmount = viewModel.LoanAmount.ToArrayIfNull(),
                Lvr = viewModel.Lvr.ToArrayIfNull(),
                RepaymentMethods = viewModel.RepaymentMethods.ToArrayIfNull().Select(p => (Enums.RepaymentMethodsEnum)Enum.Parse(typeof(Enums.RepaymentMethodsEnum), p)),
                RepaymentFrequency = viewModel.RepaymentFrequency.ToArrayIfNull().Select(p => (Enums.RepaymentFrequencyEnum)Enum.Parse(typeof(Enums.RepaymentFrequencyEnum), p)),
                StatementCycle = viewModel.StatementCycle.ToArrayIfNull().Select(p => (Enums.StatementCycleEnum)Enum.Parse(typeof(Enums.StatementCycleEnum), p)),
                SecurityPriority = viewModel.SecurityPriority.ToArrayIfNull().Select(p => (Enums.SecurityPriorityEnum)Enum.Parse(typeof(Enums.SecurityPriorityEnum), p)),
                NumberOfSplits = viewModel.NumberOfSplits,
                GenuineSavings = viewModel.GenuineSavings,
                BaseInterestRate = viewModel.BaseInterestRate,
                LoanPurposes = viewModel.LoanPurposes.ToArrayIfNull(),
                Features = viewModel.Features.ToArrayIfNull()
            };
        }
    }
}
