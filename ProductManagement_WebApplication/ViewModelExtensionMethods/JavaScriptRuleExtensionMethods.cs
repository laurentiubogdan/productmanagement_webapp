﻿using ProductManagement.Api.Requests.Rule;
using ProductManagement.Api.Responses.Rule;
using ProductManagement_WebApplication.ViewModels.Rules;

namespace ProductManagement_WebApplication.ViewModelExtensionMethods
{
    public static class JavaScriptRuleExtensionMethods
    {
        public static JavaScriptRuleViewModel ToJavaScriptRuleViewModel(this JavaScriptRuleResponse response)
        {
            return new JavaScriptRuleViewModel
            {
                Id = response.Id,
                Name = response.Name,
                Description = response.Description,
                Rule = response.Rule,
                RuleType = response.RuleType
            };
        }

        public static JavaScriptRuleCreateRequest ToJavaScriptRuleCreateRequest(this CreateJavaScriptRuleViewModel model)
        {
            return new JavaScriptRuleCreateRequest
            {
                Name = model.Name,
                Description = model.Description,
                Rule = model.Rule,
                RuleType = model.RuleType
            };
        }
    }
}
