﻿using ProductManagement.Api.Helpers;
using ProductManagement.Api.Responses.Feature;
using ProductManagement_WebApplication.ViewModels;

namespace ProductManagement_WebApplication.ViewModelExtensionMethods
{
    public static class FeatureExtensionMethods
    {
        public static FeatureViewModel ToFeatureViewModel(this FeatureResponse response)
        {
            return new FeatureViewModel
            {
                Id = response.Id,
                Name = response.Name,
                Description = response.Description,
                Type = response.Type.ToStringWithSpaces(),
                Status = response.Status,
                Rules = response.Rules
            };
        }

        public static ProductFeatureViewModel ToProductFeatureViewModel(this ProductFeatureResponse response)
        {
            return new ProductFeatureViewModel
            {
                Id = response.Feature.Id,
                Name = response.Feature.Name,
                Description = response.Feature.Description,
                Type = response.Feature.Type.ToStringWithSpaces(),
                Status = response.Status,
                FeatureRules = response.Feature.Rules,
            };
        }
    }
}
