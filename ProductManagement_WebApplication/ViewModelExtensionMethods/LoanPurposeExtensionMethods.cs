﻿using Kendo.Mvc.UI.Fluent;
using ProductManagement.Api.Helpers;
using ProductManagement.Api.Requests.BaseProduct;
using ProductManagement.Api.Responses.BaseProduct;
using ProductManagement.Api.Responses.LoanPurpose;
using ProductManagement.Domain.Helper_Objects;
using ProductManagement_WebApplication.ViewModels;
using ProductManagement_WebApplication.ViewModels.BaseProduct;
using ProductManagement_WebApplication.ViewModels.LoanPurpose;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ProductManagement_WebApplication.ViewModelExtensionMethods
{
    public static class LoanPurposeExtensionMethods
    {


        public static LoanPurposeViewModel ToLoanPurposeViewModel(this LoanPurposeResponse response)
        {
            return new LoanPurposeViewModel
            {
                Id = response.Id,
                Name = response.Name,
            };
        }
    }
}
