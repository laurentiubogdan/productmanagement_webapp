﻿using ProductManagement.Api.Helpers;
using ProductManagement.Api.Responses.Policy;
using ProductManagement_WebApplication.ViewModels;

namespace ProductManagement_WebApplication.ViewModelExtensionMethods
{
    public static class PolicyExtensionMethods
    {
        public static PolicyViewModel ToPolicyViewModel(this PolicyResponse response)
        {
            return new PolicyViewModel
            {
                Id = response.Id,
                Name = response.Name,
                Description = response.Description,
                Type = response.Type.ToStringWithSpaces(),
                Status = response.Status,
                Items = response.Items,
                Rules = response.Rules
            };
        }
        public static ProductPolicyViewModel ToProductPolicyViewModel(this ProductPolicyResponse response)
        {
            return new ProductPolicyViewModel
            {
                Id = response.Policy.Id,
                Name = response.Policy.Name,
                Description = response.Policy.Description,
                Type = response.Policy.Type.ToStringWithSpaces(),
                Status = response.Status,
                Items = response.Policy.Items,
                PolicyRules = response.Policy.Rules,
            };
        }
    }
}
