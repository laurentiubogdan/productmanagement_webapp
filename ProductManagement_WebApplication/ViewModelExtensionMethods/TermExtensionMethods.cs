﻿using ProductManagement.Api.Requests.Terms;
using ProductManagement.Domain.Helper_Objects;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ProductManagement_WebApplication.ViewModelExtensionMethods
{
    public static class TermExtensionMethods
    {
        public static TermUpdateRequest ToTermRequest(this IEnumerable<Term> terms)
        {
            if (terms.Count() == 0 || terms == null)
            {
                return new TermUpdateRequest {
                    Values = Enumerable.Empty<int>(),
                    Frequency = null
                };
            }

            var intArray = new List<int>();

            foreach (Term term in terms)
            {
                intArray.Add(term.Value);
            }
            return new TermUpdateRequest
            {
                Values = intArray,
                Frequency = (Enums.TermFrequencyEnum)Enum.Parse(typeof(Enums.TermFrequencyEnum), terms.FirstOrDefault().Frequency)
            };
        }
    }
}
