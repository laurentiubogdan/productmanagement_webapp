﻿using ProductManagement.Api.Requests.BaseProduct;
using ProductManagement_WebApplication.GenericClasses;
using ProductManagement_WebApplication.ViewModels.BaseProduct;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProductManagement_WebApplication.Requests
{
    public class RenderPartialRequest
    {
        public string RootToPartial { get; set; }
        public string ModelType { get; set; }
        public object Model { get; set; }
    }
}
