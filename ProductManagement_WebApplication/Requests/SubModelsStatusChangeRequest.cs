﻿using System.Collections.Generic;

namespace ProductManagement_WebApplication.Requests
{
    public class SubModelsStatusChangeRequest
    {
        public string ModelId { get; set; }
        public IEnumerable<string> SubModelIds { get; set; }
        public string Status { get; set; }
    }
}
