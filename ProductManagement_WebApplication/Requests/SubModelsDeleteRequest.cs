﻿using System.Collections.Generic;

namespace ProductManagement_WebApplication.Requests
{
    public class SubModelsDeleteRequest
    {
        public string ModelId { get; set; }
        public IEnumerable<string> SubModelIds { get; set; }
    }
}
