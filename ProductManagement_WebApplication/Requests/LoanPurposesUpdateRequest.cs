﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProductManagement_WebApplication.Requests
{
    public class LoanPurposesUpdateRequest
    {
        public IEnumerable<string> LoanPurposesIds { get; set; }
    }
}
