﻿using System.Collections.Generic;

namespace ProductManagement_WebApplication.Requests
{
    public class SubModelsAddRequest
    {
        public string ModelId { get; set; }
        public IEnumerable<string> SubModelIds { get; set; }
    }
}
