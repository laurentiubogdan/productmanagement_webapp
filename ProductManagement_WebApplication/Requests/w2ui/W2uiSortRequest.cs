﻿
namespace ProductManagement_WebApplication.Requests.w2ui
{
    public class W2uiSortRequest
    {
        public string Field { get; set; }
        public string Direction { get; set; }
    }
}
