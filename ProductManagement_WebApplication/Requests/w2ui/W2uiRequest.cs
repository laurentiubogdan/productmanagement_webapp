﻿using ProductManagement.Api.Requests;
using System.Collections.Generic;

namespace ProductManagement_WebApplication.Requests.w2ui
{
    public class W2uiRequest
    {
        public string Cmd { get; set; }
        public IEnumerable<int> Selected { get; set; }
        public int Offset { get; set; }
        public int Limit { get; set; }
        public IEnumerable<FilterRequest> FilterRequests { get; set; }
        public IEnumerable<SortRequest> Sort { get; set; }
    }
}
