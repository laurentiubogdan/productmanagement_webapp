﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProductManagement_WebApplication.Requests.w2ui
{
    public class W2uiSearchRequest
    {
        public string Field { get; set; }
        public string Operator { get; set; }
        public IEnumerable<string> Value { get; set; }
    }
}
