﻿using System.Collections.Generic;

namespace ProductManagement_WebApplication.Requests
{
    public class ModelsDeleteRequest
    {
        public IEnumerable<string> ModelIds { get; set; }
    }
}
