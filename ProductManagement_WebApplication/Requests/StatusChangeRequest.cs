﻿using System.Collections.Generic;

namespace ProductManagement_WebApplication.Requests
{
    public class StatusChangeRequest
    {
        public IEnumerable<string> ModelIds { get; set; }
        public string Status { get; set; }
    }
}
