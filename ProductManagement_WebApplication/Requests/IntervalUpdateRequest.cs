﻿using ProductManagement.Domain.Helper_Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProductManagement_WebApplication.Requests
{
    public class IntervalUpdateRequest
    {
        public string Intervals { get; set; }
    }
}
