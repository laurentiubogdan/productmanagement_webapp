﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProductManagement_WebApplication.Requests
{
    public class FeaturesUpdateRequest
    {
        public IEnumerable<string> FeaturesIds { get; set; }
    }
}
