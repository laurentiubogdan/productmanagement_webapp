﻿/// <reference path="../lib/w2ui/w2ui-1.5.rc1.min.js" />

requirejs.config({
    baseUrl: '/',
    paths: {
        "jquery": "node_modules/jquery/dist/jquery.min",
        //"jquery-validation": "lib/jquery-validation/dist/jquery.validate.min",
        //"jquery-validation-unobtrusive": "lib/jquery-validation-unobtrusive/jquery.validate.unobtrusive.min",
        //"jquery-unobtrusive-ajax": "node_modules/jquery-ajax-unobtrusive",
        "bootstrap": "node_modules/bootstrap/dist/js/bootstrap.bundle.min",
        "multiselect": "node_modules/bootstrap-multiselect/dist/js/bootstrap-multiselect",
        "jquerySortable": "node_modules/jquery-ui-sortable-npm/jquery-ui-sortable.min",
        "knockout": "node_modules/knockout/build/output/knockout-latest",
        "knockout-mapping": "node_modules/knockout-mapping/dist/knockout.mapping.min",
        "jszip": "node_modules/jszip/dist/jszip.min",
        "fileSaver": "node_modules/file-saver/dist/filesaver.min",
        "codemirror":"node_modules/codemirror/src/codemirror",
        "w2ui":"lib/w2ui/w2ui-1.5.rc1"
    },
    config: {
        text: {
            useXhr: function () {
                // allow cross-domain requests
                // remote server allows CORS
                return false;
            }
        }
    },
    packages: [{
        name: "codemirror",
        location: "node_modules/codemirror",
        main: "lib/codemirror"
    }]
});

(function () {
    var load = requirejs.load;
    requirejs.load = function (context, moduleId, url) {
        if (moduleId && moduleId.startsWith("kendo.") || moduleId && moduleId.startsWith("jszip.")) {
            // if moduleId starts with "kendo.", we overwrite the url path to the kendo library location
            url = url.substring(url.lastIndexOf("/") + 1);
            url = "/node_modules/kendo-ui/js/" + url;
        }
        load.apply(this, arguments);
    };
}());