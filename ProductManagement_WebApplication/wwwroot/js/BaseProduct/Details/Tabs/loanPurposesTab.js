﻿"use strict"
define(["jquery", "js/Plugins/PageHeader", "js/Plugins/DeleteModal", "js/Plugins/SideModal", "multiselect"],
    function ($, PageHeader, DeleteModal, SideModal) {
        var grid = null,
            getGridSelectedRowsPropertiesValue = null,
            getGridSelectedRowsData = null,
            loanPurposes = null,
            options = {
                viewModel: {
                    Create: {
                        include: true,
                        text: "Add Loan Purposes",
                        click: openAddLoanPurposesModal

                    },
                    Delete: {
                        include: true,
                        text: "Remove",
                        click: function () {
                            openRemoveModal();
                        },
                        isDisabled: true
                    },
                    Data: {
                        include: true,
                        text: "Data"
                        //click: openDataSideModal
                    }
                }
            },
            pageHeader = new PageHeader(options);

        // Append grid helper methods to this javascript
        require(["js/Shared/gridHelpers"], function (gridHelpersJs) {
            getGridSelectedRowsPropertiesValue = function (grid, properties) {
                return gridHelpersJs.getGridSelectedRowsPropertiesValue(grid, properties);
            };
            getGridSelectedRowsData = function (grid) {
                return gridHelpersJs.getGridSelectedRowsData(grid);
            }
        });

        reCheck();

        function reCheck() {
            $.ajax({
                url: "/BaseProduct/GridListLoanPurposes",
                type: "Post",
                data: {
                    baseProductId: BaseProduct.Id
                }
            })
                .done(function (data) {
                    loanPurposes = data;

                    if (data.length > 0) {
                        require(["lib/w2ui/w2ui-1.5.rc1.min"], function () {
                            if (grid == null) {
                                $.when(init()).done(function () {
                                    grid = w2ui["baseProductLoanPurposesGrid"];
                                });
                            } else {
                                grid.reload();
                            }
                        });
                        $("#nav-loanPurposes").toggleClass("centered-tab-pane", false);
                        $(".loan-purposes-template-container").remove();


                        $("#nav-loanPurposes-tab").on("show.bs.tab", function () {
                            pageHeader.Rebind(options);
                            if (grid) {
                                checkPageHeaderButtons();
                            }
                        });
                    } else {
                        if (grid) {
                            grid.destroy();
                            pageHeader.viewModel.Delete.isDisabled(true);
                            grid = null
                        }
                        var templateHtml = $($("#loan-purposes-template").html())
                        $("#nav-loanPurposes").append(templateHtml);

                        $("#nav-loanPurposes").toggleClass("centered-tab-pane", true);

                        $(".add-loan-purposes").on("click", openAddLoanPurposesModal);
                    }
                });
        }


        function init() {
            w2utils.settings.dataType = "HTTP"

            grid = $('#baseProductLoanPurposesGrid').w2grid({
                name: 'baseProductLoanPurposesGrid',
                url: '/BaseProduct/GridListLoanPurposes',
                postData: {
                    baseProductid: BaseProduct.Id
                },
                limit: 50,
                fixedBody: true,
                recordHeight: 40,
                recid: 'Id',
                show: {
                    selectColumn: true
                },
                columns: [
                    { field: 'Name', caption: 'Name', size: '100%', sortable: true }
                ],
                onLoad: function (event) {
                    var numberOfRecords = JSON.parse(event.xhr.responseText).length;
                    if (numberOfRecords > 15) {
                        this.fixedBody = true;

                        $("#baseProductLoanPurposesGrid").css("height", "650px");
                        this.resize();
                    }
                    event.onComplete = function () {
                        if (numberOfRecords <= 15) {
                            this.fixedBody = false;
                        }
                    }
                }
            });

            ['select', 'unselect'].forEach(evt =>
                grid.on(evt, function (event) {
                    checkPageHeaderButtons(event)
                })
            );
        }


        // Open Add Loan Purposes Modal
        function openAddLoanPurposesModal() {
            var sideModal = new SideModal({
                pathUrl: "/BaseProduct/RenderAddLoanPurposesPartial",
                modalId: "addLoanPurposesModal",
                modalTitle: "Add Loan Purposes",
                onPartialAppended: function () {
                    var $select = $("#loanPurposes-select");

                    $select.multiselect({
                        numberDisplayed: 2,
                        buttonClass: "btn button form-control",
                        maxHeight: 200,
                        onInitialized: function ($select) {
                            $.ajax({
                                url: "/LoanPurpose/ListLoanPurposes",
                                type: "Get",
                                success: function (response) {
                                    var data = [],
                                        loanPurposesIds = loanPurposes.map(loanPurpose => loanPurpose.Id);

                                    $.each(response, function () {
                                        var loanPurpose = this;

                                        // jump to next iteration
                                        if (loanPurposesIds.includes(loanPurpose.Id)) {
                                            return;
                                        }

                                        data.push({
                                            label: loanPurpose.Name,
                                            value: loanPurpose.Id
                                        })
                                    });

                                    $select.multiselect("dataprovider", data);
                                }
                            });
                        },
                    });
                },
                mainButtonHandler: function () {
                    $.ajax({
                        url: "/BaseProduct/AddLoanPurposes",
                        type: "POST",
                        data: {
                            baseProductId: BaseProduct.Id,
                            request: {
                                LoanPurposesIds: $("#loanPurposes-select").val()
                            }
                        },
                        success: function () {
                            sideModal.Destroy();
                            reCheck();
                        }
                    });
                }
            });
            sideModal.Open();
        }

        // Open the Remove Modal with the given user options
        function openRemoveModal() {
            var modalInfo = {
                entityType: "Base Product Loan Purposes",
                requestData: {
                    ModelId: BaseProduct.Id,
                    SubModelIds: getGridSelectedRowsPropertiesValue(grid, new Array("Id"))
                }
            },
                deleteModal = new DeleteModal({
                    modalInfo: modalInfo,
                    requestUrl: "/BaseProduct/RemoveLoanPurposes",
                    onDelete: function () {
                        reCheck()
                    }
                });
            deleteModal.Open();
        }

        function checkPageHeaderButtons(event) {
            if (event) {
                event.onComplete = function () {
                    check();
                }
            } else {
                check();
            }

            function check() {
                var selectedRowsIds = grid.getSelection();

                if (selectedRowsIds.length > 0) {
                    pageHeader.viewModel.Delete.isDisabled(false);
                } else {
                    pageHeader.viewModel.Delete.isDisabled(true);

                }
            }
        }
    });