﻿"use strict"
define(["jquery", "js/Plugins/PageHeader", "js/Plugins/SideModal", "js/BaseProduct/Details/splitsHelper", "kendo.grid.min"],
    function ($, PageHeader, SideModal, splitsHelper) {
        var sideModal = null,
            options = {
                viewModel: {
                    Create: {
                        include: true,
                        text: "Add Rule",
                        click: function () {
                            openAddRuleModal();
                        }
                    },
                    Delete: {
                        include: true,
                        text: "Remove",
                        click: function () {
                            var $grid = $("#baseProductSplitsRules").data("kendoGrid");
                            openDeleteRulesModal(splitsHelper.FilterArray(splitsHelper.GetGridSelectedRowsData($grid), "Id"));
                        }
                    },
                    Data: {
                        include: true,
                        text: "Data"
                        //click: openDataSideModal
                    },
                    AfterRenderCallback: function () {
                        $(".delete-button").prop("disabled", false);
                    }
                }
            };

        var pageHeader = new PageHeader(options);

        //ui events
        if (BaseProductSplitsData.Rules !== null) {
            getRulesObjectById(splitsHelper.FilterArray(BaseProductSplitsData.Rules, "Id"), function (data, statusText) {
                if (statusText === "success") {
                    for (var i = 0; i < data.responseJSON.length; i++) {
                        data.responseJSON[i].Active = BaseProductSplitsData.Rules[i].Active;
                    }
                    populateRulesGrid(data.responseJSON, "baseProductSplitsRules");
                }
            });
        } else {
            BaseProductSplitsData.Rules = [];
        }

        // Event Listeners

        $("#nav-rules-tab").on("show.bs.tab", function () {
            pageHeader.Rebind(options);
        });

        $("body").on("click", "#toggleRule", function () {
            $(this).toggleClass("fa-toggle-on fa-toggle-off");
            var grid = $('#baseProductSplitsRules').data('kendoGrid');
            var tr = $(this).closest("tr");
            var selectedRow = grid.dataItem(tr);
            updateRulesAvailability(selectedRow.Id);
            getRulesObjectById(getAvailableRules(BaseProductSplitsData.Rules), function (data, statusText) {
                if (statusText === "success") {
                    splitsHelper.FilterSplitsByRules(BaseProductSplitsData, concatenateRules(data.responseJSON));
                } else {
                    splitsHelper.FilterSplitsByRules(BaseProductSplitsData, "");
                }
            });
        });

        // Functions

        function gatherDataFromAddRuleModal(optionsCodeMirror) {
            var rule = {};
            rule.Name = $("#ruleName").val();
            rule.RuleType = $("#ruleType").val();
            rule.Rule = optionsCodeMirror.getValue();
            rule.Description = $("#descriptionBaseProduct").val();

            return rule;
        }

        function getAvailableRules(rules) {
            var result = splitsHelper.FilterArray(splitsHelper.FilterArrayByPropertyValue(rules, "Active", true), "Id");
            if (result.length > 0) {
                return result;
            } else {
                return [];
            }
        }

        function concatenateRules(rules) {
            var ruleResult = "";
            rules.forEach(function (item) {
                ruleResult += " " + item.Rule;
            });

            return ruleResult;
        }

        //request functions and handle data functionss
        function requestCreateRule(ruleData) {
            $.ajax({
                type: "POST",
                url: "/JavaScriptRule/CreateAsync",
                data: ruleData,
                complete: function (data, statusText) {
                    if (statusText === "success") {
                        var rule = {};
                        rule.Id = data.responseJSON.Id;
                        rule.Active = true;
                        BaseProductSplitsData.Rules.push(rule);
                        splitsHelper.UpdateBaseProductSplits(BaseProductSplitsData);
                        getRulesObjectById(splitsHelper.FilterArray(BaseProductSplitsData.Rules, "Id"), function (data, statusText) {
                            if (statusText === "success") {
                                populateRulesGrid(data.responseJSON, "baseProductSplitsRules");
                            }
                        });
                        sideModal.Destroy();
                    }
                }
            });
        }

        function updateRulesAvailability(someId) {
            BaseProductSplitsData.Rules.forEach(function (item) {
                if (item.Id === someId) {
                    if (item.Active === true) {
                        item.Active = false;
                    }
                    else {
                        item.Active = true;
                    }
                }
            });
            splitsHelper.UpdateBaseProductSplits(BaseProductSplitsData); // update request
        }

        function getRulesObjectById(arrayOfIds, callback) {
            var requestData = {};
            requestData.FilterProperty = "Id";
            requestData.FilterValues = arrayOfIds;
            $.ajax({
                type: "POST",
                url: "/JavaScriptRule/ListByIdsAsync",
                data: requestData,
                complete: function (data, statusText) {
                    callback(data, statusText);
                }
            });
        }

        //modals functions

        function openDeleteRulesModal(rulesIds) {
            sideModal = new SideModal({
                pathUrl: "/BaseProduct/RenderRemoveRulesPartial",
                modalId: "RemoveRulesModal",
                modalTitle: "Remove Rules",
                onPartialAppended: function () {
                    $(".submitModal").on("click", function () {
                        BaseProductSplitsData.Rules = splitsHelper.RemoveObjectFromArrayByValue(BaseProductSplitsData.Rules, "Id", rulesIds);
                        splitsHelper.UpdateBaseProductSplits(BaseProductSplitsData);
                        getRulesObjectById(splitsHelper.FilterArray(BaseProductSplitsData.Rules, "Id"), function (data, statusText) {
                            if (statusText === "success") {
                                populateRulesGrid(data.responseJSON, "baseProductSplitsRules");
                            }
                        });
                        sideModal.Destroy();
                    });
                }
            });
            sideModal.Open();
        }

        function openAddRuleModal() {
            sideModal = new SideModal({
                pathUrl: "/BaseProduct/RenderCreateRulePartial",
                modalId: "CreateRuleModal",
                modalTitle: "Add Rule",
                onPartialAppended: function () {
                    require.undef("js/BaseProduct/Details/codeMirror");
                    $('script[data-requiremodule="js/BaseProduct/Details/codeMirror"]').remove();
                    require(["js/BaseProduct/Details/codeMirror"], function (optionsCodeMirror) {
                        $(".submitModal").on("click", function () {
                            requestCreateRule(gatherDataFromAddRuleModal(optionsCodeMirror));
                        });
                    });
                }
            });
            sideModal.Open();
        }

        //populate grid function

        function populateRulesGrid(gridData, gridName) {
            $("#" + gridName).kendoGrid({
                dataSource: {
                    data: gridData,
                    schema: {
                        model: {
                            fields: {
                                Name: { type: "string" },
                                Rule: { type: "string" },
                                RuleType: { type: "string" },
                                Description: { type: "string" }
                            }
                        }
                    },
                    sort: [
                        { field: "Name", dir: "asc" }
                    ]
                },
                //height: 550,
                sortable: {
                    mode: "multiple"
                },
                pageable: {
                    refresh: true,
                    pageSizes: true,
                    buttonCount: 5
                },
                columns: [
                    {
                        selectable: true,
                        width: "50px"
                    },
                    {
                        field: "Name",
                        title: "Name"
                    },
                    {
                        field: "Rule",
                        title: "Rule",
                        width: 750
                    },
                    {
                        field: "RuleType",
                        title: "RuleType"
                    },
                    {
                        field: "Description",
                        title: "Description"
                    },
                    {
                        template: function (dataItem) {
                            if (dataItem.Active === false) {
                                return "<i id='toggleRule' class='switchStatusRule fas fa-toggle-off fa-lg'></i>";
                            } else {
                                return "<i id='toggleRule' class='switchStatusRule fas fa-toggle-on fa-lg'></i>";
                            }
                        }

                    }
                ]
            });
        }

        return {
        };
    });
