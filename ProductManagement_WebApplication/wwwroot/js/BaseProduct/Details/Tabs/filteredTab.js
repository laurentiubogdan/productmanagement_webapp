﻿"use strict"
define(["jquery", "js/Plugins/PageHeader", "js/BaseProduct/Details/splitsHelper", "kendo.grid.min"],
    function ($, PageHeader, splitsHelper) {
        var options = {
            viewModel: {
                SetStatus: {
                    include: true,
                    text: "Set Status",
                    click: function () {
                        //openStatusChangeSideModal();
                    }
                },
                Data: {
                    include: true,
                    text: "Data"
                    //click: openDataSideModal
                },
                AfterRenderCallback: function () {
                }
            }
        };

        var pageHeader = new PageHeader(options);

        // Event Listeners
        $("#nav-filtered-tab").on("show.bs.tab", function () {
            pageHeader.Rebind(options);
        });


        //ui grid loader
        splitsHelper.PopulateGrid(BaseProductSplitsData.FilteredSplits, "filteredSplits");

        return {
        };
    });