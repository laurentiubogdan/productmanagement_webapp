﻿"use strict"
define(["jquery", "js/Plugins/PageHeader", "js/Plugins/SideModal", "js/BaseProduct/Details/splitsHelper", "kendo.grid.min"],
    function ($, PageHeader, SideModal, splitsHelper) {
        var options = {
            viewModel: {
                GenerateSplits: {
                    include: true,
                    text: "Generate Splits",
                    click: function () {
                        openGenerateSplitsModal();
                    }
                },
                SetStatus: {
                    include: true,
                    text: "Set Status",
                    //click: function () {
                    //    openRemoveModal();
                    //}
                },
                Data: {
                    include: true,
                    text: "Data"
                    //click: openDataSideModal
                },
                AfterRenderCallback: function () {
                }
            }
        };

        var pageHeader = new PageHeader(options);

        // Event Listeners

        $("#nav-splits-tab").on("show.bs.tab", function () {
            pageHeader.Rebind(options);
        });

        //ui grid loader
        splitsHelper.PopulateGrid(BaseProductSplitsData.Splits, "totalSplits");

        function openGenerateSplitsModal() {
            var sideModal = new SideModal({
                pathUrl: "/BaseProduct/RenderGenerateSplitsPartial",
                modalId: "GenerateSplitsModal",
                modalTitle: "Generate Splits",
                onPartialAppended: function () {
                    require.undef("js/BaseProduct/Details/splits");
                    $('script[data-requiremodule="js/BaseProduct/Details/splits"]').remove();
                    require(["js/BaseProduct/Details/splits"], function (splitsGenerator) {
                        $(".submitModal").on("click", function () {
                            splitsGenerator.generateSplits();
                        });
                    });
                }
            });
            sideModal.Open();
        }

        return {
        };
    });
