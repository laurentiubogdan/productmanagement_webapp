﻿"use strict"
define(["jquery", "js/Plugins/PageHeader", "js/Plugins/DeleteModal", "js/Plugins/SideModal", "multiselect"],
    function ($, PageHeader, DeleteModal, SideModal) {
        var grid = null,
            getGridSelectedRowsPropertiesValue = null,
            getGridSelectedRowsData = null,
            features = null,
            options = {
                viewModel: {
                    Create: {
                        include: true,
                        text: "Add Features",
                        click: openAddFeaturesModal

                    },
                    Delete: {
                        include: true,
                        text: "Remove",
                        click: function () {
                            openRemoveModal();
                        },
                        isDisabled: true
                    },
                    Data: {
                        include: true,
                        text: "Data"
                        //click: openDataSideModal
                    }
                }
            },
            pageHeader = new PageHeader(options);

        // Append grid helper methods to this javascript
        require(["js/Shared/gridHelpers"], function (gridHelpersJs) {
            getGridSelectedRowsPropertiesValue = function (grid, properties) {
                return gridHelpersJs.getGridSelectedRowsPropertiesValue(grid, properties);
            };
            getGridSelectedRowsData = function (grid) {
                console.log(grid);
                return gridHelpersJs.getGridSelectedRowsData(grid);
            }
        });

        reCheck()

        function reCheck() {
            $.ajax({
                url: "/BaseProduct/GridListFeatures",
                type: "Post",
                data: {
                    baseProductId: BaseProduct.Id
                }
            })
                .done(function (data) {
                    features = data;

                    if (data.length > 0) {
                        require(["lib/w2ui/w2ui-1.5.rc1.min"], function () {
                            if (grid == null) {
                                $.when(init()).done(function () {
                                    grid = w2ui["baseProductFeaturesGrid"];
                                });
                            } else {
                                grid.reload();
                            }
                        });
                        $("#nav-features").toggleClass("centered-tab-pane", false);
                        $(".features-template-container").remove();


                        $("#nav-features-tab").on("show.bs.tab", function () {
                            pageHeader.Rebind(options);
                            if (grid) {
                                checkPageHeaderButtons();
                            }
                        });
                    } else {
                        if (grid) {
                            grid.destroy();
                            pageHeader.viewModel.Delete.isDisabled(true);
                            grid = null
                        }
                        var templateHtml = $($("#features-template").html())
                        $("#nav-features").append(templateHtml);

                        $("#nav-features").toggleClass("centered-tab-pane", true);

                        $(".add-features").on("click", openAddFeaturesModal);
                    }
                });
        }


        function init() {
            w2utils.settings.dataType = "HTTP"

            grid = $('#baseProductFeaturesGrid').w2grid({
                name: 'baseProductFeaturesGrid',
                url: '/BaseProduct/GridListFeatures',
                postData: {
                    baseProductid: BaseProduct.Id
                },
                limit: 50,
                fixedBody: true,
                recordHeight: 40,
                recid: 'Id',
                show: {
                    selectColumn: true
                },
                columns: [
                    { field: `Name`, caption: 'Name', size: '25%', sortable: true, },
                    { field: 'Type', caption: 'Type', size: '25%', sortable: true },
                    { field: 'Description', caption: 'Description', size: '25%', sortable: true },
                    { field: 'Status', caption: 'Status', size: '25%', sortable: true }
                ],
                onLoad: function (event) {
                    var numberOfRecords = JSON.parse(event.xhr.responseText).length;
                    if (numberOfRecords > 8) {
                        this.fixedBody = true;

                        $("#grid").css("height", "40px");
                        this.resize();
                    }
                    event.onComplete = function () {
                        if (numberOfRecords < 8) {
                            this.fixedBody = false;
                        }
                    }
                }
            });

            ['select', 'unselect'].forEach(evt =>
                grid.on(evt, function (event) {
                    checkPageHeaderButtons(event)
                })
            );
        }


        // Open Add Loan Purposes Modal
        function openAddFeaturesModal() {
            var sideModal = new SideModal({
                pathUrl: "/BaseProduct/RenderAddFeaturesPartial",
                modalId: "addFeaturesModal",
                modalTitle: "Add Features",
                onPartialAppended: function () {
                    var $select = $("#features-select");

                    $select.multiselect({
                        numberDisplayed: 2,
                        buttonClass: "btn button form-control",
                        maxHeight: 200,
                        onInitialized: function ($select) {
                            var queryRequest = {
                                projectionRequest: {
                                    ProjectionFields: ["Name"]
                                }
                            };
                            $.ajax({
                                url: "/Feature/ListFeatures",
                                type: "POST",
                                data: queryRequest,
                                success: function (response) {
                                    var data = [],
                                        featuresIds = features.map(feature => feature.Id);

                                    $.each(response, function () {
                                        var feature = this;

                                        // jump to next iteration
                                        if (featuresIds.includes(feature.Id)) {
                                            return;
                                        }

                                        data.push({
                                            label: feature.Name,
                                            value: feature.Id
                                        })
                                    });

                                    $select.multiselect("dataprovider", data);
                                }
                            });
                        },
                    });
                },
                mainButtonHandler: function () {
                    $.ajax({
                        url: "/BaseProduct/AddFeatures",
                        type: "POST",
                        data: {
                            baseProductId: BaseProduct.Id,
                            request: {
                                FeaturesIds: $("#features-select").val()
                            }
                        },
                        success: function () {
                            sideModal.Destroy();
                            reCheck();
                        }
                    });
                }
            });
            sideModal.Open();
        }

        // Open the Remove Modal with the given user options
        function openRemoveModal() {
            var modalInfo = {
                entityType: "Base Product Features",
                requestData: {
                    ModelId: BaseProduct.Id,
                    SubModelIds: getGridSelectedRowsPropertiesValue(grid, new Array("Id"))
                }
            },
                deleteModal = new DeleteModal({
                    modalInfo: modalInfo,
                    requestUrl: "/BaseProduct/RemoveFeatures",
                    onDelete: function () {
                        reCheck()
                    }
                });
            deleteModal.Open();
        }

        function checkPageHeaderButtons(event) {
            if (event) {
                event.onComplete = function () {
                    check();
                }
            } else {
                check();
            }

            function check() {
                var selectedRowsIds = grid.getSelection();

                if (selectedRowsIds.length > 0) {
                    pageHeader.viewModel.Delete.isDisabled(false);
                } else {
                    pageHeader.viewModel.Delete.isDisabled(true);

                }
            }
        }
    });