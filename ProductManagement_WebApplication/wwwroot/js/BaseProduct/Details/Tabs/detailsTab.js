﻿"use strict"
define(["jquery", "js/Plugins/PageHeader", "js/Plugins/DeleteModal", "js/Plugins/SideModal"],
    function ($, PageHeader, DeleteModal, SideModal) {
        var detailsLink = $(".details-link"),
            options = {
                viewModel: {
                    SetStatus: {
                        include: true,
                        text: "Set Status",
                        click: function () {
                            //openStatusChangeSideModal();
                        }
                    },
                    Delete: {
                        include: true,
                        text: "Delete",
                        click: function () {
                            openDeleteModal();
                        }
                    },
                    Data: {
                        include: true,
                        text: "Data"
                        //click: openDataSideModal
                    },
                    AfterRenderCallback: function () {
                        var $setStatusButton = $(".set-status-button");
                        var $deleteButton = $(".delete-button");
                        var $dataButton = $(".data-button");

                        $setStatusButton.prop("disabled", false);
                        $deleteButton.prop("disabled", false);
                    }
                }
            };

        var pageHeader = new PageHeader(options);

        // Event listeners
        detailsLink.on("click", function () {
            openUpdateSideModal($(this).attr("id"));
        });
        $("#nav-details-tab").on("show.bs.tab", function () {
            pageHeader.Rebind(options);
        });

        // Functions
        function openUpdateSideModal(clickedLabel) {
            var sideModal = new SideModal({
                pathUrl: "/BaseProduct/RenderUpdateBaseProductPartial",
                model: BaseProduct,
                modalId: "updateBaseProductModal",
                internalScrollbar: true,
                modalTitle: "Edit Base Product Details",
                deleteButton: true,
                onPartialAppended: function () {
                    require.undef("js/BaseProduct/Update/update");
                    $('script[data-requiremodule="js/BaseProduct/Update/update"]').remove();
                    require(["js/BaseProduct/Update/update"], function (updateJs) {
                        $(".deleteButtonEdit").on("click", function () {
                            openDeleteModal();
                        });
                        $(`#${sideModal.options.modalId}`).on('shown.bs.modal', function () {
                            updateJs.focusOnElement(clickedLabel);
                        });
                    });
                },
                onModalDestroyed: function () {
                },
                mainButtonHandler: function () {
                    $("#updateBaseProductForm").submit();
                }
            });

            sideModal.Open();
        }

        function openDeleteModal() {
            var modalInfo = {
                entityType: "Product",
                requestData: {
                    ModelIds: BaseProduct.Id
                }
            },
                deleteModal = new DeleteModal({
                    modalInfo: modalInfo,
                    requestUrl: "/BaseProduct/Delete",
                    onDelete: function () {
                        window.location.href = "/BaseProduct/BaseProducts";
                    }
                });
            deleteModal.Open();
        }

        return {
        };
    });