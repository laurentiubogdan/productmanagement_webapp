﻿"use strict"
define(["jquery", "kendo.grid.min"], function ($) {
    var splitsHelper = {};

    splitsHelper.PopulateGrid = function (gridData, gridName) {
        $("#" + gridName).kendoGrid({
            dataSource: {
                data: gridData,
                schema: {
                    model: {
                        fields: {
                            Name: { type: "string" },
                            PrimaryLoanPurpose: { type: "string" },
                            DocumentationType: { type: "string" },
                            InterestOnlyTerm: { type: "array" },
                            PrepaidInterestTerm: { type: "array" },
                            FixedRateTerm: { type: "array" },
                            Lvr: { type: "array" },
                            LoanAmount: { type: "array" }
                        }
                    }
                },
                sort: [
                    { field: "Name", dir: "asc" }
                ]
            },
            //height: 550,
            sortable: {
                mode: "multiple"
            },
            pageable: {
                refresh: true,
                pageSizes: true,
                buttonCount: 5
            },
            columns: [
                {
                    selectable: true,
                    width: "50px"
                },
                {
                    field: "Name",
                    title: "Name",
                    width: 100
                },
                {
                    field: "PrimaryLoanPurpose",
                    title: "PrimaryLoanPurpose",
                    width: 250
                },
                {
                    field: "DocumentationType",
                    title: "DocumentationType",
                    width: 130
                },
                {
                    field: "InterestOnlyTerm",
                    title: "InterestOnlyTerm",
                    width: 300,
                    template: function (dataItem) {
                        var stringResult = "";
                        for (var i = 0; i < dataItem.InterestOnlyTerm.length; i++) {
                            stringResult += dataItem.InterestOnlyTerm[i].Value + '-' + dataItem.InterestOnlyTerm[i].Frequency + "; ";
                        }
                        return stringResult;
                    }
                },
                {
                    field: "PrepaidInterestTerm",
                    title: "PrepaidInterestTerm",
                    width: 190,
                    template: function (dataItem) {
                        var stringResult = "";
                        for (var i = 0; i < dataItem.PrepaidInterestTerm.length; i++) {
                            stringResult += dataItem.PrepaidInterestTerm[i].Value + '-' + dataItem.PrepaidInterestTerm[i].Frequency + "; ";
                        }
                        return stringResult;
                    }
                },
                {
                    field: "FixedRateTerm",
                    title: "FixedRateTerm",
                    width: 190,
                    template: function (dataItem) {
                        var stringResult = "";
                        for (var i = 0; i < dataItem.FixedRateTerm.length; i++) {
                            stringResult += dataItem.FixedRateTerm[i].Value + '-' + dataItem.FixedRateTerm[i].Frequency + "; ";
                        }
                        return stringResult;
                    }
                },
                {
                    field: "Lvr",
                    title: "Lvr",
                    width: 150,
                    template: function (dataItem) {
                        var stringResult = "";
                        for (var i = 0; i < dataItem.Lvr.length; i++) {
                            stringResult += dataItem.Lvr[i].Min + '-' + dataItem.Lvr[i].Max + "; ";
                        }
                        return stringResult;
                    }
                },
                {
                    field: "LoanAmount",
                    title: "LoanAmount",
                    template: function (dataItem) {
                        var stringResult = "";
                        for (var i = 0; i < dataItem.LoanAmount.length; i++) {
                            stringResult += dataItem.LoanAmount[i].Min + '-' + dataItem.LoanAmount[i].Max + "; ";
                        }
                        return stringResult;
                    }
                }
            ]
        });
    };

    splitsHelper.UpdateBaseProductSplits = function (baseFilteredData) {
        $.ajax({
            type: "POST",
            url: "/BaseProductSplits/Update",
            data: baseFilteredData,
            complete: function (data, statusText) {
                if (statusText === "success") {
                    console.log("a mers FLACAI!");
                }
            }
        });
    };

    splitsHelper.GetBaseProductSplitsData = function (callback) {
        $.ajax({
            type: "POST",
            url: "/BaseProductSplits/Get",
            data: {
                Id: BaseProduct.Id
            },
            complete: function (data, statusText) {
                callback(data, statusText);
            }
        });
    };

    splitsHelper.RequestCreateBaseSplits = function (baseData, callback) {
        $.ajax({
            type: "POST",
            url: "/BaseProductSplits/Create",
            data: {
                Id: baseData
            },
            complete: function (data, statusText) {
                callback(data, statusText);
            }
        });
    };

    splitsHelper.FilterSplitsByRules = function (baseProductSplitsData, rules) {
        $.ajax({
            type: "POST",
            url: "/BaseProductSplits/Get",
            data: {
                Id: BaseProduct.Id
            },
            complete: function (data, statusText) {
                if (statusText === "success") {
                    var filteredSplits = [];
                    data.responseJSON.Splits.forEach(split => {
                        Product = split;
                        try {
                            var filtered = new Function(rules + "return true")();
                        }
                        catch (err) {
                            alert("THE RULE IS NOT VALID");
                        }
                        if (filtered) {
                            filteredSplits.push(split);
                        }
                    });
                    baseProductSplitsData.FilteredSplits = filteredSplits;
                    splitsHelper.UpdateBaseProductSplits(baseProductSplitsData);
                    splitsHelper.PopulateGrid(baseProductSplitsData.FilteredSplits, "filteredSplits");
                }
            }
        });
    };

    splitsHelper.GetGridSelectedRowsData = function ($grid) {
        var rows = [];

        $.each($grid.select(), function () {
            var row = this;

            rows.push($grid.dataItem(row));
        });

        return rows;
    };

    splitsHelper.RefreshGrid = function (gridName) {
        $("#" + gridName).data("kendoGrid").dataSource.read();
    };

    splitsHelper.FilterArray = function (arrayOfObjects, key) {
        return arrayOfObjects.map(function (item) { return item[key]; }); // return an array of values of the selected property from an array of objects
    };

    splitsHelper.FilterArrayByPropertyValue = function (arrayOfObjects, key, value) {  // return an array of objects that has property (key) with the given value
        var resultArray = arrayOfObjects;
        resultArray = resultArray.filter(function (obj) {
            return obj[key] === value;
        });
        return resultArray;
    };

    splitsHelper.RemoveObjectFromArrayByValue = function (arrayOfObjects, key, sourceArray) { // returns an array of objects with the condition that of their properties values (key) is not in the sourceArray
        var resultArray = arrayOfObjects;
        for (var i = 0; i < sourceArray.length; i++) {
            resultArray = resultArray.filter(function (obj) {
                return obj[key] !== sourceArray[i];
            });
        }
        return resultArray;
    };

    return splitsHelper;
});