﻿"use strict"
define(["jquery", "js/BaseProduct/Details/splitsHelper", "multiselect"], function ($, splitsHelper) {

    console.log(BaseProduct);
    var $splitsSelect = $("#splitsSelector"),
        splitProperties = {},
        splitsGenerator = {};

    //Event Listeners
    $splitsSelect.multiselect({
        nonSelectedText: 'Select Splits Properties',
        enableClickableOptGroups: true
    });

    //Event listeners functions

    splitsGenerator.generateSplits = function () {
        var splits = [];
        splitProperties = {};
        var results = null;
        if ($splitsSelect.val().length < 1) {
            results = getCombinations({ Name: ["Name"] }, 0, [], {});
        } else {
            $splitsSelect.val().forEach(function (item) {
                splitProperties[item] = BaseProduct[item];
            });
            results = getCombinations(splitProperties, 0, [], {});
        }
        results.forEach(function (item) {
            var split = $.extend({}, BaseProduct, item);
            splits.push(split);
        });

        BaseProductSplitsData.Splits = splits;
        console.log(BaseProductSplitsData.Splits);
        splitsHelper.UpdateBaseProductSplits(BaseProductSplitsData);
        splitsHelper.PopulateGrid(BaseProductSplitsData.Splits, "totalSplits");
    };

    function getCombinations(options, optionIndex, results, current) {
        var allKeys = Object.keys(options);
        var optionKey = allKeys[optionIndex];
        var vals = options[optionKey];

        for (var i = 0; i < vals.length; i++) {
            current[optionKey] = [];
            current[optionKey][0] = vals[i];
            if (optionIndex + 1 < allKeys.length) {
                getCombinations(options, optionIndex + 1, results, current);
            } else {
                var res = JSON.parse(JSON.stringify(current));
                results.push(res);
            }
        }
        return results;
    }

    return splitsGenerator;
});