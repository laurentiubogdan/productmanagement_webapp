﻿"use strict"
define(["jquery", "node_modules/codemirror/lib/codemirror", "node_modules/codemirror/mode/javascript/javascript", "node_modules/codemirror/addon/edit/matchbrackets", "node_modules/codemirror/addon/selection/active-line", "node_modules/codemirror/addon/hint/show-hint", "node_modules/codemirror/addon/hint/javascript-hint"],
    function ($, CodeMirror) {
        var optionsCodeMirror = null;

        setSettingsCodeMirror();

        function setSettingsCodeMirror() {
            optionsCodeMirror = CodeMirror.fromTextArea($(".text-area")[0], {
                lineNumbers: true,
                styleActiveLine: true,
                matchBrackets: true,
                mode: "javascript",
                extraKeys: { "Ctrl-Space": "autocomplete" }
            });
            optionsCodeMirror.setSize(null, 150);
            optionsCodeMirror.on("keyup", function (cm, event) {
                if (!cm.state.completionActive &&   /*Enables keyboard navigation in autocomplete list*/
                    event.keyCode > 64 && event.keyCode < 91) {// only when a letter key is pressed
                    CodeMirror.commands.autocomplete(cm, null, { completeSingle: false });
                }
            });
        }

        return optionsCodeMirror;
    });