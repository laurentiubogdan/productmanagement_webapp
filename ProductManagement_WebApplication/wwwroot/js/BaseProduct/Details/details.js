﻿"use strict"
define(["jquery", "js/BaseProduct/Details/splitsHelper"],
    function ($, splitsHelper) {

        $('.main-nav[data-toggle="tab"]').on('show.bs.tab', function (e) {
            var selectedTab = $(e.target).attr("Id");
            switch (selectedTab) {
                case "nav-details-tab":
                    require(["js/BaseProduct/Details/Tabs/detailsTab"], function () {
                    });
                    break;
                case "nav-features-tab":
                    require(["js/BaseProduct/Details/Tabs/featuresTab"], function () {
                    });
                    break;
                case "nav-loanPurposes-tab":
                    require(["js/BaseProduct/Details/Tabs/loanPurposesTab"], function () {
                    });
                    break;
                case "nav-splits-tab":
                    require(["js/BaseProduct/Details/Tabs/splitsTab"], function () {
                    });
                    break;
                case "nav-rules-tab":
                    require(["js/BaseProduct/Details/Tabs/rulesTab"], function () {
                    });
                    break;
                case "nav-filtered-tab":
                    require(["js/BaseProduct/Details/Tabs/filteredTab"], function () {
                    });
                    break;
            }
        });

        $("#nav-details-tab").tab("show");

        //load BaseProductSplitsData
        splitsHelper.GetBaseProductSplitsData(function (data, statusText) {
            if (statusText === "success") {
                BaseProductSplitsData = data.responseJSON;
            } else {
                splitsHelper.RequestCreateBaseSplits(BaseProduct.Id, function (data, statusText) {
                    if (statusText === "success") {
                        BaseProductSplitsData = data.responseJSON;
                        BaseProductSplitsData.Rules = [];
                    }
                });
            }
        });
    }
);