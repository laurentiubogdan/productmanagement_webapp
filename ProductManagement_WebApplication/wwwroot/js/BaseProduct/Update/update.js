﻿"use strict"
define(["knockout", "jquery", "js/Plugins/SideModal", "multiselect"],
    function (ko, $, SideModal, multiselect) {

        const $nameProduct = $("#name-input"),
            $codeProduct = $("#productCode-input"),
            $funderSelect = $("#funder-select"),
            $descriptionProduct = $("#description-input"),
            $documentationTypeSelect = $("#documentationType-select"),
            $primaryLoanPurposeSelect = $("#primaryLoanPurpose-select"),
            $principalInterestTermSelect = $("#principalInterestTerm-select"),
            $interestOnlyTermSelect = $("#interestOnlyTerm-select"),
            $prepaidInterestTermSelect = $("#prepaidInterestTerm-select"),
            $interestCapitalisedTermSelect = $("#interestCapitalisedTerm-select"),
            $principalInterestFeesTermSelect = $("#principalInterestFeesTerm-select"),
            $variableRateTermSelect = $("#variableRateTerm-select"),
            $fixedRateTermSelect = $("#fixedRateTerm-select"),
            $principalInterestTermInput = $(".principalInterestTerm-input"),
            $interestOnlyTermInput = $(".interestOnlyTerm-input"),
            $prepaidInterestTermInput = $(".prepaidInterestTerm-input"),
            $interestCapitalisedTermInput = $(".interestCapitalisedTerm-input"),
            $principalInterestFeesTermInput = $(".principalInterestFeesTerm-input"),
            $variableRateTermInput = $(".variableRateTerm-input"),
            $fixedRateTermInput = $(".fixedRateTerm-input"),
            $loanAmountInput = $(".loanAmount-input"),
            $lvrInput = $(".lvr-input"),
            $baseInterestRateInput = $(".baseInterestRate-input"),
            $repaymentMethodsSelect = $("#repaymentMethods-select"),
            $repaymentFrequencySelect = $("#repaymentFrequency-select"),
            $statementCycleSelect = $("#statementCycle-select"),
            $securityPrioritySelect = $("#securityPriority-select"),
            $numberOfSplitsInput = $("#numberOfSplits-input");

        $.each($(".form-multiselect"), function () {
            const $multiSelect = $(this);
            $multiSelect.multiselect({
                buttonClass: "btn button form-control"
            });
        });

        Object.defineProperty(String.prototype, "ToCapitalized", {
            value: function ToCapitalized() {
                return this.charAt(0).toUpperCase() + this.slice(1);
            },
            writable: false,
            configurable: true
        });

        //Event listeners
        $("#nav-advancedSettings-tab").on('show.bs.tab', termInputSelectOnLoadCheck);
        $(".term-input").on("change", function () {
            termInputSelectCheck(this);
        });
        $principalInterestTermInput.on("keydown", function (e) {
            checkTermValue(e)
        });
        $loanAmountInput.on("click", openLoanAmountModal);
        $lvrInput.on("click", openLvrModal);
        $baseInterestRateInput.on("change", checkBaseInterestRateValue);

        //Event listeners methods

        // get select value if enabled or null if disabled
        function getTermFrequency($select) {
            const $selectButton = $select.next().children(".multiselect.dropdown-toggle.btn");

            if ($selectButton.prop('disabled')) {
                return null
            } else {
                return $select.val();
            }
        }

        // disable multiselects if input value is empty when clicking on advanced settings tab for the first time
        function termInputSelectOnLoadCheck() {
            $.each($(".term-input"), function () {
                termInputSelectCheck(this);
            });
            $("#nav-advancedSettings-tab").off('show.bs.tab', termInputSelectOnLoadCheck);
        }

        // disable multiselects if input value is empty or enable if populated
        function termInputSelectCheck(input) {
            const $input = $(input),
                stringFromId = $input[0].id.split("-")[0],
                $selectButton = $(`#${stringFromId}-select`).next().children(".multiselect.dropdown-toggle.btn");

            if ($input.val() == "") {
                $selectButton.prop("disabled", true);
            } else {
                $selectButton.prop("disabled", false);
            }
        }

        // Loan amount modal initiate
        function openLoanAmountModal() {
            const IntervalUpdateRequest = {
                Intervals: $loanAmountInput.val()
            },
                sideModal = new SideModal({
                    $container: $(".plugin-container-extra"),
                    pathUrl: "/BaseProduct/RenderUpdateBaseProductIntervalPartial",
                    model: IntervalUpdateRequest,
                    modalId: "intervalsModal",
                    internalScrollbar: true,
                    modalTitle: "Edit Loan Amount Intervals",
                    onPartialAppended: function () {
                        require.undef("js/BaseProduct/Update/intervals");
                        $('script[data-requiremodule="js/BaseProduct/Update/intervals"]').remove();
                        require(["js/BaseProduct/Update/intervals"], function (intervalsJs) {
                            $(".submitModal", "#intervalsModal").on("click", function () {
                                const result = intervalsJs.getResult();
                                $loanAmountInput.val(result);
                                sideModal.Destroy();
                            });
                        });
                    }
                });
            sideModal.Open();
        }

        // Lvr modal initiate
        function openLvrModal() {
            const IntervalUpdateRequest = {
                Intervals: $lvrInput.val()
            },
                sideModal = new SideModal({
                    $container: $(".plugin-container-extra"),
                    pathUrl: "/BaseProduct/RenderUpdateBaseProductIntervalPartial",
                    model: IntervalUpdateRequest,
                    modalId: "intervalsModal",
                    internalScrollbar: true,
                    modalTitle: "Edit Lvr Intervals",
                    onPartialAppended: function () {
                        require.undef("js/BaseProduct/Update/intervals");
                        $('script[data-requiremodule="js/BaseProduct/Update/intervals"]').remove();
                        require(["js/BaseProduct/Update/intervals"], function (intervalsJs) {
                            $(".submitModal", "#intervalsModal").on("click", function () {
                                const result = intervalsJs.getResult();
                                $lvrInput.val(result);
                                sideModal.Destroy();
                            });
                        });
                    }
                });

            sideModal.Open();
        }

        // check if value has "%" or not and add if not
        function checkBaseInterestRateValue() {
            const value = $baseInterestRateInput.val();

            if (value.includes("%")) {
                $baseInterestRateInput.val(value);
            } else {
                $baseInterestRateInput.val(value.concat("%"));
            }
        }

        // get base interest rate value without "%"
        function getBaseInterestRateValue() {
            return $baseInterestRateInput.val().substring(0, $baseInterestRateInput.val().length - 1);
        }

        //update the product function - creating the object 
        $(".submitModal", "#updateBaseProductModal").on("click", function () {

            const baseProductUpdateRequest = {
                Name: $nameProduct.val().ToCapitalized(),
                ProductCode: $codeProduct.val(),
                Funder: $funderSelect.val(),
                Description: $descriptionProduct.val().ToCapitalized(),
                Type: BaseProduct.Type,
                DocumentationType: $documentationTypeSelect.val(),
                PrimaryLoanPurpose: $primaryLoanPurposeSelect.val(),
                PrincipalInterestTerm: {
                    Values: getTermValues($principalInterestTermInput),
                    Frequency: getTermFrequency($principalInterestTermSelect)
                },
                InterestOnlyTerm: {
                    Values: getTermValues($interestOnlyTermInput),
                    Frequency: getTermFrequency($interestOnlyTermSelect)
                },
                PrepaidInterestTerm: {
                    Values: getTermValues($prepaidInterestTermInput),
                    Frequency: getTermFrequency($prepaidInterestTermSelect)
                },
                InterestCapitalisedTerm: {
                    Values: getTermValues($interestCapitalisedTermInput),
                    Frequency: getTermFrequency($interestCapitalisedTermSelect)
                },
                PrincipalInterestFeesTerm: {
                    Values: getTermValues($principalInterestFeesTermInput),
                    Frequency: getTermFrequency($principalInterestFeesTermSelect)
                },
                VariableRateTerm: {
                    Values: getTermValues($variableRateTermInput),
                    Frequency: getTermFrequency($variableRateTermSelect)
                },
                FixedRateTerm: {
                    Values: getTermValues($fixedRateTermInput),
                    Frequency: getTermFrequency($fixedRateTermSelect)
                },
                LoanAmount: getIntervalValues($loanAmountInput),
                Lvr: getIntervalValues($lvrInput),
                BaseInterestRate: getBaseInterestRateValue(),
                RepaymentMethods: $repaymentMethodsSelect.val(),
                RepaymentFrequency: $repaymentFrequencySelect.val(),
                StatementCycle: $statementCycleSelect.val(),
                SecurityPriority: $securityPrioritySelect.val(),
                NumberOfSplits: $numberOfSplitsInput.val(),
                GenuineSavings: BaseProduct.GenuineSavings,
                LoanPurposes: BaseProduct.LoanPurposes
            };

            updateProduct(BaseProduct.Id, baseProductUpdateRequest);

            // interprate term input val to get individual numbers for base product update request
            function getTermValues(input) {
                const valuesToInterpret = input.val().split(","),
                    values = [];

                $.each(valuesToInterpret, function () {
                    var valueAsString = this;

                    if (valueAsString.includes("-")) {
                        const fromValueToValueValues = valueAsString.split("-");
                        let index = parseInt(fromValueToValueValues[0]);

                        for (index; index <= fromValueToValueValues[1]; index++) {
                            values.push(index);
                        }
                    } else {
                        values.push(parseInt(valueAsString));
                    }
                });
                return values;
            }
        });

        function getIntervalValues($input) {
            const inputValue = $input.val(),
                intervals = inputValue.split(", "),
                result = [];

            if (inputValue != "") {
                $.each(intervals, function (i, interval) {
                    const values = interval.split(" - "),
                        intervalObject = {};

                    $.each(values, function (i, val) {
                        const rawValue = parseInt(val.replace(/[$%,]/g, "").split(".")[0]);

                        i == 0 ? intervalObject["Min"] = rawValue : intervalObject["Max"] = rawValue;
                    });
                    result.push(intervalObject);
                });
                return result;
            } else {
                return [];
            }
        }

        //ajax call to update the product.
        function updateProduct(id, object) {
            $.ajax({
                url: "/BaseProduct/Update",
                type: "POST",
                data: {
                    id: id,
                    request: object
                },
                success: function () {
                    location.reload();
                }
            });
        }

        return {
            focusOnElement: function focusOnElement(clickedLabel) {
                const tab = clickedLabel.split("-")[0],
                    partialId = clickedLabel.split("-")[1];

                let focusOnElement = null;

                if (clickedLabel.split("-")[3]) {
                    focusOnElement = function () {
                        $(`#${partialId}-select`).next().children(".multiselect.dropdown-toggle").focus();
                    }
                } else {
                    focusOnElement = function () {
                        $(`#${partialId}-input`).focus();
                    }
                }

                switch (tab) {
                    case "firstTab":
                        focusOnElement();
                        break;
                    case "secondTab":
                        focusOnTabShown();
                        $("#nav-advancedSettings-tab").tab("show");
                        break;
                    case "thirdTab":
                        focusOnTabShown();
                        $("#nav-otherSettings-tab").tab("show");
                        break;
                }

                function focusOnTabShown() {
                    $("#nav-advancedSettings-tab, #nav-otherSettings-tab").on('shown.bs.tab', function () {
                        focusOnElement();
                        $("#nav-advancedSettings-tab, #nav-otherSettings-tab").off('shown.bs.tab');
                    })
                }
            }
        };
    });