﻿"use strict"
define(["jquery"],
    function ($) {
        var $addIntervalButton = $(".addInterval-button"),
            $deleteIntervalButton = null;

        $("#intervalsModal .modal-body").css({
            padding: "1rem",
            "overflow-x": "hidden"
        });

        deleteGarbageIconsAndAddForLast();

        // Event listeners
        $addIntervalButton.on("click", addInterval);

        // Functions

        // Add interval template
        function addInterval() {
            var existingIntervals = $(".interval-container").length;

            if (existingIntervals === 0) {
                appendTemplate();

                $($(".intervalMin-input")[existingIntervals]).focus();

                deleteGarbageIconsAndAddForLast();
            } else if ($($(".intervalMax-input")[existingIntervals - 1]).val()) {
                appendTemplate();

                $($(".intervalMin-input")[existingIntervals]).attr({
                    disabled: "true",
                    value: $($(".intervalMax-input")[existingIntervals - 1]).val()
                });
                $($(".intervalMax-input")[existingIntervals]).focus();

                deleteGarbageIconsAndAddForLast();
            }
        };

        // Interval Template Append 
        function appendTemplate() {
            var templateHtml = $($("#interval-template").html())
            $(".intervals-container").append(templateHtml);
        }

        // Delete all garbage icons and add for last input
        function deleteGarbageIconsAndAddForLast() {
            $(".fontAwesome-col").remove();

            var $newFontAwesomeDiv = $("<div/>", {
                class: "col-1 fontAwesome-col"
            }),
                $newFontAwesomeIcon = $("<i/>", {
                    class: "far fa-trash-alt"
                });

            $(".interval-input-group:last").append($newFontAwesomeDiv);
            $newFontAwesomeDiv.append($newFontAwesomeIcon);

            $deleteIntervalButton = $newFontAwesomeIcon;

            $deleteIntervalButton.on("click", function () {
                $(this).parent().parent().parent().remove();

                deleteGarbageIconsAndAddForLast();
            });
        }

        var getIntervals = function() {
            var stringArray = [];

            $.each($(".interval-input-group"), function () {
                var $containerElement = $(this);

                var minValue = $containerElement.find(".intervalMin-input").val();
                var maxValue = $containerElement.find(".intervalMax-input").val();

                stringArray.push(`${minValue} - ${maxValue}`);
            });

            return stringArray.join(", ");
        }

        return {
            getResult: getIntervals
        }
    });