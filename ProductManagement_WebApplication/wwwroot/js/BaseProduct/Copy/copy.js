﻿"use strict"
define(["jquery"],
    function ($) {
        $("#submitModal", "#copyProductModal").on("click", function () {
            if (!event.detail || event.detail === 1) {
                dataObject.Name = $("#nameProduct").val();
                dataObject.Description = $("#descriptionProduct").val();
                dataObject.Type = $("#typeProduct").val();
                if (dataObject.PaymentType[0] === "-") {
                    dataObject.PaymentType = null;
                } else {
                    dataObject.PaymentType = dataObject.PaymentType.replace(/\s/g, '').split(",");
                }
                $.ajax({
                    type: "POST",
                    url: "/Product/Copy",
                    data: {
                        model: dataObject
                    },
                    complete: function (data, statusText) {
                        if (statusText === "success") {
                            window.location.href = data.responseJSON.newurl;
                        }
                    }
                });
            }
            else { return false; }
        });
    });