﻿"use strict"
define(["jquery", "multiselect"],
    function ($) {

        var $funderSelect = $("#funder-select"),
            $typeSelect = $("#type-select");

        $funderSelect.multiselect({
            buttonClass: "btn button form-control",
        });

        $typeSelect.multiselect({
            buttonClass: "btn button form-control",
        });

        Object.defineProperty(String.prototype, "ToCapitalized", {
            value: function ToCapitalized() {
                return this.charAt(0).toUpperCase() + this.slice(1);
            },
            writable: false,
            configurable: false
        });

        $(".submitModal", "#createBaseProductModal").on("click", function () {
            var nameVal = $("#name-input").val();
            var descriptionVal = $("#description-input").val();

            if (!event.detail || event.detail === 1) {
                $("#name-input").val(nameVal.ToCapitalized());
                $("#description-input").val(descriptionVal.ToCapitalized());
                $("#createBaseProductForm").submit();
            }
            else { return false; }
        })
        
        $(".createAndNew", "#createBaseProductModal").on("click", function () {
            if (!event.detail || event.detail === 1) {
                var modelProd = {
                    Name: $("#name-input").val().ToCapitalized(),
                    ProductCode: $("#productCode-input").val(),
                    Type: $("#type-select").val(),
                    Funder: $("#funder-select").val(),
                    Description: $("#description-input").val().ToCapitalized()
                };
                $.ajax({
                    type: "POST",
                    url: "/BaseProduct/Create",
                    data: {
                        model: modelProd
                    },
                    complete: function (data, statusText) {
                        if (statusText === "success") {
                            require(["lib/w2ui/w2ui-1.5.rc1.min"], function () {
                                w2ui["grid"].reload();
                                $("#name-input").val("");
                                $("#productCode-input").val("");
                                $("#description-input").val("");
                            });
                        }
                    }
                });
            }
            else { return false; }
        })
    });