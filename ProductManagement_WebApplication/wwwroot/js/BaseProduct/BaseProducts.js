﻿"use strict";
define(["jquery", "js/Plugins/PageHeader", "js/Plugins/DeleteModal", "js/Plugins/SideModal", "js/Plugins/FilterComponent", "bootstrap"],
    function ($, PageHeader, DeleteModal, SideModal, FilterComponent) {
        require(["lib/w2ui/w2ui-1.5.rc1.min"], function () {
            init();
        });

        function init() {
            w2utils.settings.dataType = "HTTP"

            var grid = $('#grid').w2grid({
                name: 'grid',
                url: '/BaseProduct/GridListBaseProducts',
                limit: 50,
                fixedBody: true,
                recordHeight: 40,
                recid: 'Id',
                show: {
                    selectColumn: true
                },
                columns: [
                    {
                        field: 'Name', caption: 'Name', size: '300px', sortable: true,
                        render: function (record) {
                            return `<a href="/BaseProduct/Details/${record.Id}">${record.Name}</a>`
                        }
                    },
                    { field: 'Type', caption: 'Type', size: '30%', sortable: true },
                    { field: 'Description', caption: 'Description', size: '30%', sortable: true },
                ],
                onLoad: function (event) {
                    var numberOfRecords = JSON.parse(event.xhr.responseText).length;
                    if (numberOfRecords > 18) {
                        this.fixedBody = true;
                        console.log(this);
                        $("#grid").css("height", "750px");
                        this.resize();
                    }
                    event.onComplete = function () {
                        if (numberOfRecords < 18) {
                            this.fixedBody = false;
                        }
                    }
                }
            });

            var multiselectFilters = [],
                getGridSelectedRowsPropertiesValue = null,
                getGridSelectedRowsData = null;

            new PageHeader({
                viewModel: {
                    Create: {
                        include: true,
                        text: "Create Product",
                        click: openCreateSideModal
                    },
                    Copy: {
                        include: true,
                        text: "Copy Product",
                        click: openCopySideModal
                    },
                    SetStatus: {
                        include: true,
                        text: "Set Status",
                        //click: openStatusChangeSideModal
                    },
                    Delete: {
                        include: true,
                        text: "Delete",
                        click: openDeleteModal,
                        isDisabled: true
                    },
                    Data: {
                        include: true,
                        text: "Data",
                        //click: openDataSideModal
                    },
                    Search: {
                        include: true,
                        change: function (e) {
                            gridSearch(multiselectFilters);
                        }
                    }
                }
            });
            var filterComponent = new FilterComponent({
                data: [
                    { label: "All", value: "all" },
                    {
                        label: 'Status',
                        children: [
                            { label: 'Active', value: 'active' },
                            { label: 'Inactive', value: 'inactive' }
                        ]
                    },
                    {
                        label: 'Type',
                        children: [
                            { label: 'Mortgage Loan', value: 'MortgageLoan' },
                            { label: 'Personal Loan', value: 'PersonalLoan' },
                            { label: 'Credit Card', value: 'CreditCard' },
                            { label: 'Car Loan', value: 'CarLoan' },
                            { label: 'Line Of Credit', value: 'LineOfCredit' },
                            { label: 'Commercial Bill', value: 'CommercialBill' },
                            { label: 'Lease', value: 'Lease' },
                            { label: 'Margin Loan', value: 'MarginLoan' },
                            { label: 'Overdraft', value: 'Overdraft' },
                            { label: 'Reverse Mortgage', value: 'ReverseMortgage' },
                            { label: 'Term Loan', value: 'TermLoan' },
                            { label: 'Other Loan', value: 'OtherLoan' }
                        ]
                    }
                ]
            }, function (filters) {
                multiselectFilters = filters;
                gridSearch(multiselectFilters);
            });
            filterComponent.Activate();

            // Event Listeners

            ['select', 'unselect'].forEach(evt =>
                grid.on(evt, function (event) {
                    checkPageHeaderButtons(event)
                })
            );

            // Functions 

            // set the filter created to the grid 
            function gridSearch(multiSelectFilterRequests) {

                var $searchValue = $(".search-text-box").val(),
                    filterRequests = [];

                if ($searchValue != "") {
                    var baseFilter = getGridBaseFilter();

                    $.each(baseFilter, function () {
                        var filter = this;

                        filter["Values"] = [$searchValue]
                        filterRequests.push(filter);
                    });
                }

                $.each(multiSelectFilterRequests, function () {
                    var filter = this;
                    filterRequests.push(filter);
                });

                grid.postData["FilterRequests"] = filterRequests;
                grid.search();
            }

            // return the basic filter of the grid
            function getGridBaseFilter() {
                return [
                    { Property: "Name", Operator: "Contains" },
                    { Property: "Type", Operator: "Contains" }
                ];
            }

            // Open the Delete Modal with the given user options
            function openDeleteModal() {
                var modalInfo = {
                    entityType: "Base Products",
                    requestData: {
                        ModelIds: grid.getSelection()
                    }
                },
                    deleteModal = new DeleteModal({
                        modalInfo: modalInfo,
                        requestUrl: "/BaseProduct/Delete",
                        onDelete: function () {
                            w2ui['grid'].reload();
                        }
                    });
                deleteModal.Open();
            }

            //Open the Create Side Modal with the given user options
            function openCreateSideModal() {
                var sideModal = new SideModal({
                    pathUrl: "/BaseProduct/RenderCreateBaseProductPartial",
                    modalId: "createBaseProductModal",
                    modalTitle: "Create Base Product",
                    formId: "createBaseProductForm",
                    extraButton: true,
                    onPartialAppended: function () {
                        require.undef("js/BaseProduct/Create/create");
                        $('script[data-requiremodule="js/BaseProduct/Create/create"]').remove();
                        require(["js/BaseProduct/Create/create"], function () {
                        });
                        $(`#${this.modalId}`).on('shown.bs.modal', function () {
                            $("#createBaseProductForm").find(":input:first").focus();
                        });
                    }
                });
                sideModal.Open();
            }

            //Open the Copy Side Modal with the given user options
            function openCopySideModal() {
                var dataObject = JSON.parse(JSON.stringify(getGridSelectedRowsData(grid)));
                $("#typeProduct").val(dataObject.Type);

                var sideModal = new SideModal({
                    rootToPartial: "ProductPartialViews/CreateProductPartial",
                    modalId: "copyProductModal",
                    modalTitle: "Copy Product",
                    formId: "createProductForm",
                    onPartialAppended: function () {
                        require.undef("js/BaseProduct/Copy/copy");
                        $('script[data-requiremodule="js/BaseProduct/Copy/copy"]').remove();
                        require(["js/BaseProduct/Copy/copy"], function () {
                        });
                        $(`#${this.modalId}`).on('shown.bs.modal', function () {
                            $("#createProductForm").find(":input:first").focus();
                        });
                    }
                });
                sideModal.Open();
            }

            //Open the Data Side Modal with the given user options
            function openDataSideModal() {
                var sideModal = new SideModal({
                    pathUrl: "/BaseProduct/RenderDataPartial",
                    modalId: "dataModal",
                    modalTitle: "Import/Export Data",
                    mainButtonText: "Import",
                    isMainButtonEnabled: false,
                    onPartialAppended: function () {
                        // Code in NotePad in Cod important de ramas
                    }
                });
                sideModal.Open();
            }

            function checkPageHeaderButtons(event) {
                event.onComplete = function () {
                    var selectedRowsIds = this.getSelection();

                    if (selectedRowsIds.length > 0) {
                        $(".delete-button, .set-status-button, .copy-button").prop("disabled", false)
                        if (selectedRowsIds.length > 1) {
                            $(".copy-button").prop("disabled", true)
                        }
                    } else {
                        $(".delete-button, .set-status-button, .copy-button").prop("disabled", true)
                    }
                }
            }

            // Append grid helper methods to this javascript
            require(["js/Shared/gridHelpers"], function (gridHelpersJs) {
                getGridSelectedRowsPropertiesValue = function (grid, properties) {
                    return gridHelpersJs.getGridSelectedRowsPropertiesValue(grid, properties);
                };
                getGridSelectedRowsData = function (grid) {
                    return gridHelpersJs.getGridSelectedRowsData(grid);
                }
            });
        };
    });