﻿"use strict"
define(["jquery"], function ($) {

    function KendoHelper(url, vm) {
        var self = this;

        self.url = url;
        self.vm = vm

    }
    KendoHelper.prototype.Initiate = function (options) {
        var self = this;
        var $grid = options.$grid;

        if ($grid.dataSource.total() >= 100) {
            if ($(".loadMoreBtn").length == 1) {
                $(".loadMoreBtn").show();
            } else {
                var loadMore = $("<button/>", {
                    class: "loadMoreBtn",
                    text: "Load more",
                    css: { "background-color": "inherit", "border": "none", "color": "#178D8D" }
                });
                $(".page-container-content").append(loadMore);
                $(".page-container-content").on("click", ".loadMoreBtn", function () {
                    self.LoadMore(options)
                });
            }
        } else {
            $(".loadMoreBtn").hide();
        }
    }

    KendoHelper.prototype.LoadMore = function (options) {
        var self = this;

        console.log("Options", options);

        var $grid = options.$grid;
        var gridSort = $grid.dataSource.sort();
        var gridPageSize = $grid.dataSource.pageSize();
        var gridTotal = $grid.dataSource.total();
        var requestData = options.requestData;
        var skipValue = gridTotal;

        requestData["SkipValue"] = skipValue;

        var dataSource = new kendo.data.DataSource({
            type: "aspnetmvc-ajax",
            serverFiltering: true,
            serverSorting: true,
            serverPaging: true,
            transport: {
                read: {
                    url: self.url,
                    type: "POST",
                    data: requestData
                }
            },
            sort: gridSort,
            pageSize: gridPageSize + 100,
            schema: {
                total: "Total",
                data: "Data"
            }
        });

        dataSource.fetch().then(function () {
            var data = dataSource.data();
            console.log("Load More DATA", data);
            self.vm.data.push.apply(self.vm.data, data);

            console.log("Data", data);

            if (data.length == 0) {
                $(".loadMoreBtn").prop("disabled", true);
                $(".loadMoreBtn").css("color", "gray")
                $(".loadMoreBtn").text("All content was rendered");
            }
        });
    };
    return KendoHelper;
});