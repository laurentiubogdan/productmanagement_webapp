﻿"use strict";
define(["jquery", "jszip", "fileSaver"], function ($, JSZip) {

    function ExportTemplatesPlugin(options) {
        var self = this;

        ExportTemplatesPlugin.defaultOptions = {
            templatesUrls: [],
            entityType: ""
        };

        self.options = $.extend(true, {}, ExportTemplatesPlugin.defaultOptions, options);

        self.filesToDownload = self.options.templatesUrls;
        self.entityType = self.options.entityType;
    }

    ExportTemplatesPlugin.prototype.Export = function () {
        var self = this;

        if (self.filesToDownload.length > 1) {
            var zip = new JSZip(),
                request = null;

            $.each(self.filesToDownload, function () {
                var fileURL = this,
                    fileExtension = this.split('/').pop().split('.').pop();

                request = $.ajax({
                    url: fileURL,
                    type: "GET",
                    mimeType: 'text/plain; charset=x-user-defined'
                });

                request.done(function (data) {
                    zip.file(`${self.entityType}_template.${fileExtension}`, data, { binary: true });
                });
            });

            request.done(function () {
                zip.generateAsync({ type: "blob" }).then(function (data) {
                    saveAs(data, `${self.entityType }_templates.zip`);
                });
            });
        } else {
            window.location.href = self.filesToDownload[0];
        }
    };
    return ExportTemplatesPlugin;
});