﻿"use strict"
define([], function () {

    var paymentTypes = {
        PrincipalInterest: paymentTypeEnum.indexOf("PrincipalInterest"),
        InterestOnly: paymentTypeEnum.indexOf("InterestOnly")
    };

    return paymentTypes;
});