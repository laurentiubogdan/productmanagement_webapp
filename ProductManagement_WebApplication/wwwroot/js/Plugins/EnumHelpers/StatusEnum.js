﻿"use strict"
define([], function () {

    var productStatus = {
        Active: statusEnum.indexOf("Active"),
        Inactive: statusEnum.indexOf("Inactive")
    };

    return productStatus;
});