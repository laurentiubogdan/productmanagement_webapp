﻿"use strict"
define([], function () {

    var interestTypes = {
        Variable: interestTypeEnum.indexOf("Variable"),
        Fixed: interestTypeEnum.indexOf("Fixed")
    };

    return interestTypes;
});