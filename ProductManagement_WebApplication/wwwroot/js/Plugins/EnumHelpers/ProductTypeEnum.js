﻿"use strict"
define([], function () {

    var productType = {
        HomeLoan: productTypeEnum.indexOf("HomeLoan"),
        CarLoan: productTypeEnum.indexOf("CarLoan"),
        Insurance: productTypeEnum.indexOf("Insurance"),
        CreditCard: productTypeEnum.indexOf("CreditCard"),
        PersonalLoan: productTypeEnum.indexOf("PersonalLoan"),
        EquipmentFinance: productTypeEnum.indexOf("EquipmentFinance")
    };

    return productType;
});