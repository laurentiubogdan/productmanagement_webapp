﻿"use strict"
define(["jquery", "bootstrap"], function ($) {
    function DeleteModal(options) {
        var self = this;

        self.options = $.extend(true, {}, DeleteModal.DefaultOptions, options);

        // options variables
        self.$container = self.options.$container;
        self.modalInfo = self.options.modalInfo;
        self.requestUrl = self.options.requestUrl;
        self.onDelete = self.options.onDelete;

        // Creating modal elements
        self.$deleteModal = $("<div/>", {
            class: "modal fade",
            id: "deleteModal",
            tabindex: "-1",
            role: "dialog",
            "aria-hidden": "true"
        });
        self.$modalBody = $("<div/>", {
            class: "modal-body row"
        });

        var $modalDialog = $("<div/>", {
            class: "modal-dialog modal-dialog-centered",
            role: "document"
        }),
            $modalContent = $("<div/>", {
                class: "modal-content container-fluid"
            }),
            $modalFooter = $("<div/>", {
                class: "modal-footer row"
            }),
            $deleteButton = $("<button/>", {
                class: "btn button primary-button col-12 col-md-3",
                text: "Delete"
            }),
            $cancelButton = $("<button/>", {
                class: "btn button cancel-button col-12 col-md-3",
                "data-dismiss": "modal",
                text: "Cancel"
            });

        // Appending modal elements
        self.$deleteModal.append($modalDialog);
        $modalDialog.append($modalContent);
        $modalContent.append(self.$modalBody, $modalFooter);
        $modalFooter.append($cancelButton, $deleteButton);

        $deleteButton.on("click", function () {
            self.Delete();
        });

        $cancelButton.on("click", function () {
            self.Cancel();
        });

        self.$deleteModal.on('hidden.bs.modal', function () {
            this.remove();
            self.$container.children().addClass("show");
        });
    }

    DeleteModal.prototype.Open = function () {
        var self = this;

        self.$container.children().removeClass("show");
        self.$container.append(self.$deleteModal);

        self.$deleteModal.modal({
            toggle: true,
            backdrop: "static"
        });

        self.$modalBody.text(`Are you sure you want to delete the selected ${self.modalInfo.entityType.toLowerCase()} ?`);
    };

    DeleteModal.prototype.Cancel = function () {
        var self = this;
        self.$deleteModal.modal("toggle");
    };

    DeleteModal.prototype.Delete = function () {
        var self = this;
        $.ajax({
            url: self.requestUrl,
            type: "Delete",
            data: self.modalInfo.requestData,
            success: function () {
                self.onDelete();
                self.Cancel();
            }
        });
    };

    DeleteModal.DefaultOptions = {
        $container: $(".plugin-container"),
        onDelete: function () {
        }
    };

    return DeleteModal;
});