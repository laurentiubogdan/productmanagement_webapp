﻿"use strict";
define(["jquery", "knockout", "knockout-mapping"], function ($, ko) {
    function PageHeader(options) {
        var self = this;

        self.options = $.extend(true, {}, PageHeader.defaultOptions, options);
        self.viewModel = ko.mapping.fromJS(self.options.viewModel);

        // Delete binding if exists
        if (!!ko.dataFor(self.options.$container[0])) {
            ko.cleanNode(self.options.$container[0]);
        }

        ko.applyBindings(self.viewModel, self.options.$container[0]);
    }

    PageHeader.prototype.Rebind = function (options) {
        var self = this;

        self.options = $.extend(true, {}, PageHeader.defaultOptions, options);
        self.viewModel = ko.mapping.fromJS(self.options.viewModel);

        ko.cleanNode(self.options.$container[0]);
        ko.applyBindings(self.viewModel, self.options.$container[0]);
    };

    PageHeader.defaultOptions = {
        $container: $(".page-container-header"),
        $template: "PageHeader-template",
        viewModel: {
            GenerateSplits: {
                include: false,
                text: "",
                click: function () { }
            },
            Create: {
                include: false,
                text: "",
                click: function () { }
            },
            Copy: {
                include: false,
                text: "",
                click: function () { }
            },
            SetStatus: {
                include: false,
                text: "",
                click: function () { }
            },
            Delete: {
                include: false,
                text: "",
                click: function () { },
                isDisabled: false
            },
            Restore: {
                include: false,
                text: "",
                click: function () { }
            },
            Data: {
                include: false,
                text: "",
                click: function () { }
            },
            Search: {
                include: false,
                change: function () { }
            },
            AfterRenderCallback: function () { }
        }
    };

    function defaultObject() {

    }

    return PageHeader;
});