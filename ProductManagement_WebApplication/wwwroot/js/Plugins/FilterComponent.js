﻿"use strict"
define(["jquery", "bootstrap", "multiselect"], function ($) {
    function FilterComponent(configurations, callback) {
        var self = this;

        FilterComponent.defaultConfigurations = {
            $select: $("#filter-select"),
            data: [{}]
        };

        self.configurations = $.extend(true, {}, FilterComponent.defaultConfigurations, configurations);
        self.callback = callback;

        // options variables
        self.$select = self.configurations.$select;
        self.data = self.configurations.data;
    }

    FilterComponent.prototype.Activate = function () {
        var self = this;

        self.$select.multiselect({
            nonSelectedText: 'Choose to filter',
            numberDisplayed: 1,
            buttonClass: "btn button fontAwesome-element default-button",
            dropRight: true,
            onChange: function (option) {
                self.FilterChange(option);
            }
        });

        self.$select.multiselect("dataprovider", self.data);
    };

    FilterComponent.prototype.FilterChange = function (option) {
        var self = this,
            selectedOptions = self.$select.val(),
            selectedOptionsLength = selectedOptions.length;

        var optionsExceptAllLength = $("#filter-select option").length - 1;

        if (selectedOptionsLength === optionsExceptAllLength && $.inArray("all", self.selectedOptions) === -1) {
            self.$select.multiselect('deselect', selectedOptions, false);
            self.$select.multiselect('select', "all", false);
        }
        if (option.val() === "all") {
            var optionsToDelete = $.grep(selectedOptions, function (array) {
                return array !== "all";
            });

            self.$select.multiselect("deselect", optionsToDelete);
        }

        if ($.inArray("all", selectedOptions) >= 0 && option.val() !== "all") {
            self.$select.multiselect('deselect', 'all');
        }

        selectedOptions = self.$select.val();

        if ($.inArray("all", selectedOptions) >= 0) {
            selectedOptions = [];

            $.each($("#filter-select option").slice(1), function () {
                selectedOptions.push($(this).val());
            });
        }

        var filterRequests = [];

        $.each(selectedOptions, function () {
            // "this" is a primitive value of String that is returned with valueOf()
            var optionValue = this.valueOf(),
                optionOptGroup = $(`#filter-select option[value=${optionValue}]`).parent().attr("label"),
                filterRequest = filterRequests.find(request => request['Property'] === optionOptGroup)

            if (filterRequest) {
                filterRequest["Values"].push(optionValue);
            } else {
                var newFilterRequest = {
                    Property: optionOptGroup,
                    Operator: "In",
                    Values: [
                        optionValue
                    ]
                };

                filterRequests.push(newFilterRequest);
            }
        });

        self.callback(filterRequests);
    };

    return FilterComponent;
});