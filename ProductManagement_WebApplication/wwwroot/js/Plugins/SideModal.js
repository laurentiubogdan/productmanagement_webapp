﻿"use strict"
define(["jquery", "knockout", "js/Plugins/SureModal"], function ($, ko, SureModal) {

    function SideModal(options) {
        var self = this;
        $(".plugin-container").unbind("click");

        self.options = $.extend(true, {}, SideModal.defaultOptions, options);
        self.checkInputsChange = false;
        self.cancelBtnPressed = false;

        self.template = self.options.template;
        self.$container = self.options.$container;
        self.pathUrl = self.options.pathUrl;
        self.model = self.options.model;
        self.formId = self.options.formId;
        self.modalId = self.options.modalId;
        self.internalScrollbar = self.options.internalScrollbar;
        self.modalTitle = self.options.modalTitle;
        self.extraButton = self.options.extraButton;
        self.deleteButton = self.options.deleteButton;
        self.mainButtonText = self.options.mainButtonText;
        self.mainButtonHandler = self.options.mainButtonHandler;
        self.additionalButtonHandler = self.options.additionalButtonHandler;
        self.isMainButtonEnabled = self.options.isMainButtonEnabled;
        self.onPartialAppended = self.options.onPartialAppended;
        self.onModalDestroyed = self.options.onModalDestroyed;

        self.viewModel = {
            modalId: self.modalId,
            internalScrollbar: self.internalScrollbar,
            modalTitle: self.modalTitle,
            displayExtraButton: self.extraButton,
            deleteButton: self.deleteButton,
            mainButtonText: ko.observable(self.mainButtonText),
            mainButtonHandler: self.mainButtonHandler,
            additionalButtonHandler: self.additionalButtonHandler,
            isMainButtonEnabled: ko.observable(self.isMainButtonEnabled)
        };

        //console.log(self.model);

        ko.applyBindingsToNode(self.$container[0], {
            template: {
                name: self.template,
                data: self.viewModel
            }
        });
        self.$container.find("[data-bind]").removeAttr("data-bind");

        $.ajax({
            type: "Post",
            url: self.pathUrl,
            data: self.model,
            //contentType : "application/json",
            complete: function (data, statusText) {
                if (statusText === "success") {
                    $("#" + self.modalId + " .partialView-content").append(data.responseJSON.partialView);
                    self.options.onPartialAppended();
                }
            }
        });

        self.$container.on("change", ".form-control", function () {
            self.checkInputsChange = true;
        });

        $(".destroyModal", self.$container).on("click", function () {
            self.cancelBtnPressed = true;
            self.Destroy();
        });
    }

    SideModal.prototype.Open = function () {
        var self = this;
        self.$container.children().modal({
            show: true,
            backdrop: false
        });
    };

    SideModal.prototype.Destroy = function () {
        var self = this;

        if (self.checkInputsChange === true && self.cancelBtnPressed === true) {
            var sureModal = new SureModal({
                $motherModalContainer: $(self.$container)
            });
            sureModal.Open();
        } else {
            self.$container.children().remove();
            self.onModalDestroyed();
            $("body").removeClass("modal-open");
        }
    };

    SideModal.defaultOptions = {
        template: "SideModal-template",
        $container: $(".plugin-container"), // already on the DOM - pay attention when using selectors - maybe the selector doesn't exists when the js file IS LOADED, NOT when the plugin is instantiate it
        pathUrl: "/Product/RenderPartialContent",
        rootToPartial: "",
        model: {},
        formId: "",
        modalId: "",
        modalTitle: "",
        extraButton: false,
        deleteButton: false,
        mainButtonText: "Save",
        isMainButtonEnabled: true,
        onPartialAppended: function () {
        },
        onModalDestroyed: function () {
        }
    };
    return SideModal;
});