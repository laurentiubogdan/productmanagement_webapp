﻿"use strict"
define(["jquery", "knockout"], function ($, ko) {

    function SureModal(options) {
        var self = this;

        self.options = $.extend(true, {}, SureModal.defaultOptions, options);

        self.template = self.options.template;
        self.$container = self.options.$container;
        self.$motherModalContainer = self.options.$motherModalContainer;

        ko.applyBindingsToNode(self.$container[0], {
            template: {
                name: self.template,
                data: {}
            }
        });
        self.$container.find("[data-bind]").removeAttr("data-bind");

        self.$container.on("click", "#noBtn", function () {
            self.Destroy();
        });

        self.$container.on("click", "#yesBtn", function () {
            self.$motherModalContainer.children().remove();
            self.Destroy();
        });
    }

    SureModal.prototype.Open = function () {
        var self = this;
        self.$motherModalContainer.children().addClass("show");
        self.$container.children().modal({
            show: true,
            backdrop: true
        });
    };

    SureModal.prototype.Destroy = function () {
        var self = this;
        $(".modal-backdrop").remove();
        $("body").removeClass("modal-open");
        self.$container.children().remove();
    };

    SureModal.defaultOptions = {
        template: "SureModal-template",
        $container: $(".plugin-container-sureModal"),
        $motherModalContainer: $(".plugin-container")
    };

    return SureModal;
});