﻿"use strict"
define(["jquery", "multiselect", "bootstrap"], function ($) {
    function AddPlugin(options) {
        var self = this;

        self.selectedOptions = [];

        // options variables
        self.$select = options.$select;
        self.$grid = options.$grid;
        self.addRequestUrl = options.addRequestUrl;
        self.listEntitiesUrl = options.listEntitiesUrl;
        self.entityType = options.entityType;
        self.$addButton = options.$addButton;
    }

    AddPlugin.prototype.Activate = function () {
        var self = this;

        var filterRequest = {
            FilterProperty: "Status",
            FilterValues: ["Active"]
        };

        $.ajax({
            url: self.listEntitiesUrl,
            type: "Get",
            traditional: true,
            data: filterRequest,
            success: function (response) {

                var multiSelectOptions = [
                    {
                        label: null,
                        children: []
                    }
                ];

                self.$select.multiselect({
                    nonSelectedText: `Select ${self.entityType.toLowerCase()}`,
                    maxHeight: 200,
                    buttonClass: "btn btn-primary button secondary-button long-multiselect",
                    enableFiltering: true,
                    enableCaseInsensitiveFiltering: true,
                    onChange: function (option) {

                        self.selectedOptions = self.$select.val();

                        var selectedOptionsLength = self.selectedOptions.length,
                            optionsExceptAllLength = $("#content-dropdown option").length - 1;

                        if (self.selectedOptions.length === 0) {
                            self.$addButton.prop("disabled", true);
                        } else {
                            self.$addButton.prop("disabled", false);
                        }

                        if (selectedOptionsLength === optionsExceptAllLength && $.inArray("all", self.selectedOptions) === -1) {
                            console.log("a intrat");
                            self.$select.multiselect('deselect', self.selectedOptions, false);
                            self.$select.multiselect('select', "all", false);
                        }

                        if (option.val() === "all") {
                            var optionsToDelete = $.grep(self.selectedOptions, function (array) {
                                return array !== "all";
                            });

                            self.$select.multiselect("deselect", optionsToDelete);
                        }

                        if ($.inArray("all", self.selectedOptions) >= 0 && option.val() !== "all") {
                            self.$select.multiselect('deselect', 'all');
                        }

                        self.selectedOptions = self.$select.val();

                        if ($.inArray("all", self.selectedOptions) >= 0) {
                            self.selectedOptions = [];

                            $.each($("#content-dropdown option").slice(1), function () {
                                self.selectedOptions.push($(this).val());
                            });
                        }
                    }
                });

                if (response.length === 0) {
                    multiSelectOptions[0].label = `There aren't any available ${self.modalInfo.entityType.toLowerCase()} to be added for this product`;
                } else {
                    var gridDataSource = self.$grid.dataSource.data(),
                        gridDataSourceIds = [],
                        multiSelectChildrens = [];

                    $.each(gridDataSource, function () {
                        var row = this;

                        gridDataSourceIds.push(row.Id);
                    });

                    $.each(response, function () {
                        var object = this;

                        if ($.inArray(object.Id, gridDataSourceIds) === -1) {
                            multiSelectChildrens.push({ label: object.Name, value: object.Id, status: object.Status });
                        }
                    });

                    if (multiSelectChildrens.length === 0) {
                        multiSelectOptions[0].label = `There aren't any available ${self.entityType.toLowerCase()} to be added for this product`;
                    } else {
                        multiSelectOptions = [
                            {
                                label: "All",
                                value: "all"
                            },
                            {
                                label: `Available ${self.entityType}...`,
                                children: multiSelectChildrens
                            }
                        ];
                        multiSelectOptions[1].label = `Available ${self.entityType}...`;
                        multiSelectOptions[1].children = multiSelectChildrens;
                    }
                }
                self.$select.multiselect("dataprovider", multiSelectOptions);
            }
        });
    };

    AddPlugin.prototype.Add = function () {
        var self = this,
            requestData = {
                ModelId: viewModel.Id,
                SubModelIds: self.selectedOptions
            };

        $.ajax({
            url: self.addRequestUrl,
            type: "Post",
            traditional: true,
            data: JSON.stringify(requestData),
            contentType: 'application/json',
            success: function () {
                self.$grid.dataSource.read();
            }
        });
    };

    return AddPlugin;
});