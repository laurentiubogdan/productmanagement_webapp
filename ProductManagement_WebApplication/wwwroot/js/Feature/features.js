﻿define(["jquery", "js/Plugins/DeleteModal", "js/Plugins/StatusChangeModal", "js/Plugins/FilterComponent", "kendo.grid.min", "multiselect"],
    function ($, DeleteModal, StatusChangeModal, FilterComponent) {

        function implementGrid() {
            var grid = $("#grid").kendoGrid({
                dataSource: {
                    transport: {
                        read: "/Feature/GridListFeatures"
                    },
                    sort: [
                        { field: "Name", dir: "asc" }
                    ],
                    pageSize: 20
                },
                height: 650,
                sortable: {
                    mode: "multiple"
                },
                pageable: {
                    refresh: true,
                    pageSizes: true,
                    buttonCount: 5
                },
                columns: [
                    {
                        selectable: true,
                        width: "50px"
                    },
                    {
                        field: "Id",
                        title: "Id",
                        hidden: true
                    },
                    {
                        field: "Name",
                        title: "Name",
                        template: "<a href='/Feature/Details/${data.Id}'>${data.Name}</a>",
                        width: "450px"
                    },
                    {
                        field: "Type",
                        title: "Type",
                        width: "200px"
                    },
                    {
                        field: "Description",
                        title: "Description",
                        width: "600px"
                    },
                    {
                        field: "Status",
                        title: "Status"
                    }
                ]
            });
            return grid;
        }

        $.when(implementGrid()).done(function () {

            var $grid = $("#grid").data("kendoGrid"),
                $searchTextBox = $("#search-text-box"),
                $deleteButton = $(".delete-dropdown-item"),
                $activateButton = $(".activate-dropdown-item"),
                $inactivateButton = $(".inactivate-dropdown-item");

            var filterComponent = new FilterComponent({
                data: [
                    { label: "All", value: "all" },
                    {
                        label: 'Status',
                        children: [
                            { label: 'Active', value: 'active' },
                            { label: 'Inactive', value: 'inactive' }
                        ]
                    },
                    {
                        label: 'Type',
                        children: [
                            { label: 'General', value: 'general' },
                            { label: 'Home Loan', value: 'homeLoan' },
                            { label: 'Internet', value: 'internet' }
                        ]
                    }
                ]
            }, function ($grid, multiselectFilters) {
                gridSearch($grid, multiselectFilters);
            });
            filterComponent.Activate();

            $("#collapseConfiguration").collapse("show");

            //Event Listeners
            $grid.bind("change", onGridChange);
            $grid.bind('dataBound', onGridDataBound);
            $searchTextBox.on("input", function () {
                gridSearch($grid, multiselectFilters);
            });
            $deleteButton.on("click", openDeleteModal);
            $activateButton.on("click", function () {
                openStatusChangeModal("Active");
            });
            $inactivateButton.on("click", function () {
                openStatusChangeModal("Inactive");
            });

            //Event Listeners Functions

            // enables/disable the dropdown list options based on the selected grid rows
            function onGridChange() {
                var $grid = this,
                    ids = getGridSelectedRowsPropertiesValue($grid, new Array("Id"));
                if (ids.length > 0) {
                    if ($.inArray("Active", getGridSelectedRowsPropertiesValue($grid, new Array("Status"))) >= 0) {
                        changeElementsDisableStatus(new Array($inactivateButton), false);
                    } else {
                        changeElementsDisableStatus(new Array($inactivateButton), true);
                    }
                    if ($.inArray("Inactive", getGridSelectedRowsPropertiesValue($grid, new Array("Status"))) >= 0) {
                        changeElementsDisableStatus(new Array($activateButton), false);
                    } else {
                        changeElementsDisableStatus(new Array($activateButton), true);
                    }
                    changeElementsDisableStatus(new Array($deleteButton), false);
                } else {
                    changeElementsDisableStatus(new Array($deleteButton, $activateButton, $inactivateButton), true);
                }
            }

            // resets the dropdown list
            function onGridDataBound() {
                changeElementsDisableStatus(new Array($deleteButton, $activateButton, $inactivateButton), true);
                $("#grid tr.k-alt").removeClass("k-alt");
            }

            // set the filter created to the grid 
            function gridSearch($grid, multiselectFilters) {

                console.log(multiselectFilters);

                var searchValue = $.trim($searchTextBox.val()),
                    gridFilter = [],
                    baseFilter = getGridBaseFilter(),
                    modifiedFilter = [];

                $.each(baseFilter, function () {
                    var filter = this;

                    filter["value"] = searchValue;
                    modifiedFilter.push(filter);
                });

                var normalFilter = {
                    logic: "or",
                    filters: modifiedFilter
                };

                gridFilter.push(normalFilter);

                $.each(multiselectFilters, function () {
                    var filter = this;

                    gridFilter.push(filter);
                });

                $grid.dataSource.filter(gridFilter);
            }

            // Open the Delete Modal with the given user options
            function openDeleteModal() {
                var modalInfo = {
                    entityType: "Features",
                    requestData: {
                        ModelIds: getGridSelectedRowsPropertiesValue($grid, new Array("Id"))
                    },
                    requestDetails: getSelectedEntitiesData()
                };
                var deleteModal = new DeleteModal({
                    $grid: $grid,
                    modalInfo: modalInfo,
                    requestUrl: "/Feature/Delete"
                });
                deleteModal.Open();
            }

            // Open the Status Change Modal with the given user options
            function openStatusChangeModal(status) {
                var statusChangeRequest = {
                    Ids: getGridSelectedRowsPropertiesValue($grid, new Array("Id")),
                    Status: status
                },
                    modalInfo = {
                        entityType: "Features",
                        requestData: statusChangeRequest,
                        requestDetails: getSelectedEntitiesData()
                    },
                    statusChangeModal = new StatusChangeModal({
                        $grid: $grid,
                        modalInfo: modalInfo,
                        requestUrl: "/Feature/StatusChange"
                    });
                statusChangeModal.Open();
            }

            // returns an array of entity data objects
            function getSelectedEntitiesData() {
                var rows = getGridSelectedRowsData($grid),
                    selectedEntitiesData = [];

                $.each(rows, function () {
                    var row = this,
                        entityData = {},
                        entityName = row.Name,
                        entityVersion = row.Version;

                    entityData["Name"] = entityName;
                    entityData["Version"] = entityVersion;

                    selectedEntitiesData.push(entityData);
                });
                return selectedEntitiesData;
            }

            // change the disabled attribute of the elements
            function changeElementsDisableStatus($elements, disableStateBool) {
                $.each($elements, function () {
                    var $element = this;

                    $element.prop("disabled", disableStateBool);
                });
            }

            // returns an array of grid rows with its associated data
            function getGridSelectedRowsData($grid) {
                var rows = [];

                $.each($grid.select(), function () {
                    var row = this;

                    rows.push($grid.dataItem(row));
                });

                return rows;
            }

            // returns an array of properties values
            function getGridSelectedRowsPropertiesValue($grid, properties) {
                var rows = getGridSelectedRowsData($grid),
                    values = [];

                $.each(rows, function () {
                    var row = this,
                        rowProperties = [];

                    if (properties.length === 1) {
                        values.push(row[properties[0]]);
                    } else {
                        $.each(properties, function () {
                            var property = this;

                            rowProperties.push(row[property]);
                        });
                        values.push(rowProperties);
                    }
                });
                return values;
            }

            // return the basic filter of the grid
            function getGridBaseFilter() {
                var baseFilter = [
                    { field: "Name", operator: "contains" },
                    { field: "Description", operator: "contains" }
                ];
                return baseFilter;
            }
        });
    });