﻿"use strict"
define(["jquery"],
    function ($) {

        // returns an array of grid rows with its associated data
        function getGridSelectedRowsData(grid) {
            var rows = [];
            console.log(grid);
            $.each(grid.getSelection(), function () {
                var id = this;

                rows.push(grid.get(id));
            });

            return rows;
        }

        // returns an array of properties values
        function getGridSelectedRowsPropertiesValue(grid, properties) {
            var rows = getGridSelectedRowsData(grid),
                values = [];

            $.each(rows, function () {
                var row = this,
                    rowProperties = [];

                if (properties.length === 1) {
                    values.push(row[properties[0]]);
                } else {
                    $.each(properties, function () {
                        var property = this;

                        rowProperties.push(row[property]);
                    });
                    values.push(rowProperties);
                }
            });
            return values;
        }

        return {
            getGridSelectedRowsPropertiesValue: function (grid, properties) {
                return getGridSelectedRowsPropertiesValue(grid, properties);
            },
            getGridSelectedRowsData: function (grid) {
                return getGridSelectedRowsData(grid);
            }
        }
    });