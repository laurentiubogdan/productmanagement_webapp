﻿"use strict"
define(["jquery", "bootstrap"], function ($) {

    $("#toggle-sidebar").on("click", function () {
        if ($(window).width() < 769) {
            if ($(".side-menu").hasClass("show-sidebar")) {
                $(".side-menu").removeClass("show-sidebar");
                $(".side-menu").addClass("hide-sidebar");
                $(".page-container-header").css("padding-left", "38px");
                $(".list-group-item-text").hide();

            }
            else {
                $(".side-menu").addClass("show-sidebar");
                $(".side-menu").removeClass("hide-sidebar");
                $(".page-container-header").css("padding-left", "0");
                $(".list-group-item-text").show();
            }
        }
        else {
            if ($(".side-menu").hasClass("hide-sidebar")) {
                $(".side-menu").removeClass("hide-sidebar");
                $(".side-menu").addClass("show-sidebar");
                $(".page-container-header").css("padding-left", "0");
                $(".list-group-item-text").show();

            }
            else {
                $(".side-menu").addClass("hide-sidebar");
                $(".side-menu").removeClass("show-sidebar");
                $(".page-container-header").css("padding-left", "38px");
                $(".list-group-item-text").hide();
            }
        }
        $(".page-container").css("width", "100%");
    });

    $("#configuration").on("click", function () {
        $("#toggleMenuArrow").toggleClass("fa-chevron-down fa-chevron-up");
    });

    $.event.special.touchstart =
        {
            setup: function (_, ns, handle) {
                if (ns.includes("noPreventDefault")) {
                    this.addEventListener("touchstart", handle, { passive: false });
                }
                else {
                    this.addEventListener("touchstart", handle, { passive: true });
                }
            }
        };

    $.event.special.wheel =
        {
            setup: function (_, ns, handle) {
                if (ns.includes("noPreventDefault")) {
                    this.addEventListener("wheel", handle, { passive: false });
                }
                else {
                    this.addEventListener("wheel", handle, { passive: true });
                }
            }
        };

    Array.prototype.Has = function (string) {
        if (this.indexOf(string) > -1)
            return true;
        else
            return false;
    };

    Array.prototype.Value = function () {
        return this[0];
    };

    Array.prototype.Add = function (string) {
        this.push(string);
    };

    Number.prototype.Between = function (min, max) {
        if (this >= min && this <= max) {
            return true;
        }
        else {
            return false;
        }
    };
});