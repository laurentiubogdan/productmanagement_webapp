﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProductManagement_WebApplication.ViewModels.LoanPurpose
{
    public class LoanPurposeViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
