﻿using ProductManagement.Api.Responses.Rule;
using System.Collections.Generic;

namespace ProductManagement_WebApplication.ViewModels
{
    public class ProductFeatureViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Type { get; set; }
        public string Status { get; set; }
        public IEnumerable<JavaScriptRuleResponse> FeatureRules { get; set; }
    }
}
