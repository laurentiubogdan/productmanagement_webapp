﻿using ProductManagement.Domain.Helper_Objects;

namespace ProductManagement_WebApplication.ViewModels.BaseProduct
{
    public class CreateBaseProductViewModel
    {
        public string Name { get; set; }
        public string ProductCode { get; set; }
        public Enums.ProductTypeEnum Type { get; set; }
        public Enums.FunderEnum Funder { get; set; }
        public string Description { get; set; }
    }
}
