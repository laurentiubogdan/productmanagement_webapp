﻿using ProductManagement.Domain.Helper_Objects;
using System;
using System.Collections.Generic;

namespace ProductManagement_WebApplication.ViewModels.BaseProduct
{
    public class UpdateBaseProductViewModel
    {

        public string Name { get; set; }
        public string ProductCode { get; set; }
        public string Description { get; set; }
        public string Funder { get; set; }
        public string Type { get; set; }
        public string DocumentationType { get; set; }
        public string PrimaryLoanPurpose { get; set; }
        public IEnumerable<Term> PrincipalInterestTerm { get; set; }
        public IEnumerable<Term> InterestOnlyTerm { get; set; }
        public IEnumerable<Term> PrepaidInterestTerm { get; set; }
        public IEnumerable<Term> InterestCapitalisedTerm { get; set; }
        public IEnumerable<Term> PrincipalInterestFeesTerm { get; set; }
        public IEnumerable<Term> VariableRateTerm { get; set; }
        public IEnumerable<Term> FixedRateTerm { get; set; }
        public IEnumerable<Interval> LoanAmount { get; set; }
        public IEnumerable<Interval> Lvr { get; set; }
        public string RepaymentMethods { get; set; }
        public string RepaymentFrequency { get; set; }
        public string StatementCycle { get; set; }
        public string SecurityPriority { get; set; }
        public int NumberOfSplits { get; set; }
        public int GenuineSavings { get; set; }
        public int BaseInterestRate { get; set; }
        public bool ConstructionHouse { get; set; }
        public bool ConstructionOtherDwelling { get; set; }
        public bool ConstructionRenovations { get; set; }
        public bool ConstructionImprovements { get; set; }
        public bool PurchaseNewHouse { get; set; }
        public bool PurchaseNewDwelling { get; set; }
        public bool PurchaseEstablishedHouse { get; set; }
        public bool PurchaseEstablishedDwelling { get; set; }
        public bool PurchaseVacantLand { get; set; }
        public bool RefinanceHomeLoans { get; set; }
        public bool RefinancePersonalLoans { get; set; }
        public bool DebtConsolidationOfBills { get; set; }
        public bool EquityReleaseNewCar { get; set; }
        public bool EquityReleaseUsedCar { get; set; }
        public bool EquityReleaseOtherMotorVehicle { get; set; }
        public bool EquityReleaseMotorCycle { get; set; }
        public bool EquityReleaseBoatsCaravansTrailers { get; set; }
        public bool EquityReleaseHouseholdPersonal { get; set; }
        public bool EquityReleaseTravelHolidays { get; set; }
        public bool InvestmentPropertyPurchase { get; set; }
        public bool InvestmentOtherPurchase { get; set; }
        public bool InvestmentPropertyRefinance { get; set; }
        public bool OwnerBuilder { get; set; }
        public bool LicensedBuilder { get; set; }
        public IEnumerable<ProductFeatureLite> Features { get; set; }
    }
}
