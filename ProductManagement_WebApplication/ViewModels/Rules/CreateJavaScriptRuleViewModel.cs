﻿namespace ProductManagement_WebApplication.ViewModels.Rules
{
    public class CreateJavaScriptRuleViewModel
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Rule { get; set; }
        public string RuleType { get; set; }
    }
}
