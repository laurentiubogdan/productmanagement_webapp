﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json.Serialization;
using ProductManagement.Api.ApiMethods;
using ProductManagement.Api.ApiMethodsInterfaces;
using ProductManagement.Api.Helpers;
using ProductManagement_WebApplication.HelpersController.Product;
using System.Net.Http;

namespace ProductManagement_WebApplication
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<IISOptions>(options =>
            {
                options.ForwardClientCertificate = false;
            });

            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            services.AddScoped<IFeatureApiMethods, FeatureApiMethods>();
            services.AddScoped<IPolicyApiMethods, PolicyApiMethods>();
            services.AddScoped<IFeeApiMethods, FeeApiMethods>();
            services.AddScoped<IProductControllerHelper, ProductControllerHelper>();
            services.AddScoped<IBaseProductApiMethods, BaseProductApiMethods>();
            services.AddScoped<IBaseProductSplitsApiMethods, BaseProductSplitsApiMethods>();
            services.AddScoped<IJavaScriptRuleApiMethods, JavaScriptRuleApiMethods>();
            services.AddScoped<ILoanPurposeApiMethods, LoanPurposeApiMethods>();
            services.AddScoped<BackendUrlApi>(p => new BackendUrlApi { BackendUrl = this.Configuration.GetValue<string>("BackendUrl") });

            services.AddMvc()
                            .SetCompatibilityVersion(CompatibilityVersion.Version_2_2)
                            .AddJsonOptions(options =>
                                                options.SerializerSettings.ContractResolver = new DefaultContractResolver());

            services.AddHttpClient();
            services.AddKendo();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=BaseProduct}/{action=BaseProducts}/{id?}");
            });
        }
    }
}
