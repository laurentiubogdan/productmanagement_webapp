﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProductManagement_WebApplication.GenericClasses
{
    public class GenericObject<T>
    {
        public T Result { get; set; }
    }
}
