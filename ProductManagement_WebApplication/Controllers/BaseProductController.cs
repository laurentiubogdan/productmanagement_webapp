﻿using Kendo.Mvc.Extensions;
using Microsoft.AspNetCore.Mvc;
using ProductManagement.Api.ApiMethodsInterfaces;
using ProductManagement.Api.Helpers;
using ProductManagement.Api.Requests;
using ProductManagement.Api.Requests.BaseProduct;
using ProductManagement.Api.Responses.BaseProduct;
using ProductManagement.Api.Responses.Feature;
using ProductManagement.Api.Responses.LoanPurpose;
using ProductManagement.Domain.Helper_Objects;
using ProductManagement_WebApplication.HelpersController.Product;
using ProductManagement_WebApplication.Requests;
using ProductManagement_WebApplication.Requests.w2ui;
using ProductManagement_WebApplication.ViewModelExtensionMethods;
using ProductManagement_WebApplication.ViewModels;
using ProductManagement_WebApplication.ViewModels.BaseProduct;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProductManagement_WebApplication.Controllers
{
    public class BaseProductController : Controller
    {
        public readonly IBaseProductApiMethods _baseProductApiMethods;
        private readonly IProductControllerHelper _controllerHelper;


        public BaseProductController(IBaseProductApiMethods baseProductApiMethods, IProductControllerHelper controllerHelper)
        {
            _baseProductApiMethods = baseProductApiMethods;
            _controllerHelper = controllerHelper;
        }

        [HttpPost]
        public async Task<IActionResult> GridListBaseProducts(W2uiRequest request)
        {
            QueryRequest queryRequest = new QueryRequest
            {
                ProjectionRequest = new ProjectionRequest
                {
                    ProjectionFields = new string[] { "Name", "Description", "Type" }
                },
                FilterRequests = request.FilterRequests,
                SortRequests = request.Sort,
                Limit = request.Limit,
                Skip = request.Offset
            };

            ApiResult<IEnumerable<BaseProductResponse>> apiResponse = await _baseProductApiMethods.ListAsync(queryRequest);

            if (apiResponse.HasException)
            {
                return BadRequest();
            }

            IEnumerable<BaseProductGridModel> result = apiResponse.Result.Select(p => p.ToBaseProductGridModel());

            return Ok(result);
        }

        [HttpPost]
        public async Task<IActionResult> GridListLoanPurposes(W2uiRequest request, string baseProductId)
        {
            QueryRequest queryRequest = new QueryRequest
            {
                ProjectionRequest = new ProjectionRequest
                {
                    ProjectionFields = new string[] { "Name" }
                },
                SortRequests = request.Sort,
                Limit = request.Limit,
                Skip = request.Offset
            };

            ApiResult<IEnumerable<LoanPurposeResponse>> apiResponse = await _baseProductApiMethods.ListLoanPurposes(baseProductId, queryRequest);

            if (apiResponse.HasException)
            {
                return BadRequest();
            }

            IEnumerable<LoanPurposeResponse> result = apiResponse.Result;

            return Ok(result);
        }

        [HttpPost]
        public async Task<IActionResult> GridListFeatures(W2uiRequest request, string baseProductId)
        {
            QueryRequest queryRequest = new QueryRequest
            {
                ProjectionRequest = null,
                SortRequests = request.Sort,
                Limit = request.Limit,
                Skip = request.Offset
            };

            ApiResult<IEnumerable<ProductFeatureResponse>> apiResponse = await _baseProductApiMethods.ListFeatures(baseProductId, queryRequest);

            if (apiResponse.HasException)
            {
                return BadRequest();
            }

            IEnumerable<ProductFeatureViewModel> result = apiResponse.Result.Select(p => p.ToProductFeatureViewModel());

            return Ok(result);
        }

        [HttpGet]
        public IActionResult BaseProducts()
        {
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> Details(string id)
        {
            ApiResult<BaseProductResponse> apiResponse = await _baseProductApiMethods.GetAsync(id);

            if (apiResponse.HasException)
            {
                return BadRequest();
            }

            BaseProductViewModel baseProductModel = apiResponse.Result.ToBaseProductViewModel();

            return View(baseProductModel);
        }

        [HttpGet]
        public async Task<IActionResult> BaseProductDetails(string id)
        {
            ApiResult<BaseProductResponse> apiResponse = await _baseProductApiMethods.GetAsync(id);

            if (apiResponse.HasException)
            {
                return BadRequest();
            }
            BaseProductViewModel baseProductModel = apiResponse.Result.ToBaseProductViewModel();

            return Ok(baseProductModel);
        }

        [HttpPost]
        public async Task<IActionResult> Create(CreateBaseProductViewModel model)
        {
            ApiResult<BaseProductResponse> apiResponse = await _baseProductApiMethods.CreateAsync(model.ToBaseProductCreateRequest());
            if (apiResponse.HasException)
            {
                return BadRequest();
            }

            return RedirectToAction("Details", "BaseProduct", new { id = apiResponse.Result.Id, });
        }

        [HttpPost]
        public async Task<IActionResult> Update(string id, BaseProductUpdateRequest request)
        {
            ApiResult<BaseProductResponse> apiResponse = await _baseProductApiMethods.UpdateAsync(id, request);

            if (apiResponse.HasException)
            {
                return BadRequest();
            }

            return RedirectToAction("Details", "BaseProduct", new { id });
        }

        [HttpDelete]
        public async Task<IActionResult> Delete(ModelsDeleteRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            ApiResult<bool> apiResponse = await _baseProductApiMethods.DeleteManyAsync(request.ModelIds);

            if (apiResponse.HasException)
            {
                return BadRequest();
            }
            return Ok();
        }

        [HttpPost]
        public async Task<IActionResult> AddLoanPurposes(string baseProductId, LoanPurposesUpdateRequest request)
        {
            ApiResult<BaseProductResponse> getApiResponse = await _baseProductApiMethods.GetAsync(baseProductId);

            if (getApiResponse.HasException)
            {
                return NotFound();
            }

            BaseProductUpdateRequest baseProductUpdateRequest = getApiResponse.Result.ToBaseProductUpdateRequest();

            baseProductUpdateRequest.LoanPurposes = baseProductUpdateRequest.LoanPurposes.Concat(request.LoanPurposesIds);

            ApiResult<BaseProductResponse> updateApiResponse1 = await _baseProductApiMethods.UpdateAsync(baseProductId, baseProductUpdateRequest);

            if (updateApiResponse1.HasException)
            {
                return BadRequest();
            }
            return Ok();
        }

        [HttpDelete]
        public async Task<IActionResult> RemoveLoanPurposes(SubModelsDeleteRequest request)
        {
            ApiResult<BaseProductResponse> apiResponse1 = await _baseProductApiMethods.GetAsync(request.ModelId);

            if (apiResponse1.HasException)
            {
                return BadRequest();
            }

            BaseProductResponse baseProduct = apiResponse1.Result;

            IEnumerable<string> remainingLoanPurposes = baseProduct.LoanPurposes.Where(p => request.SubModelIds.All(s => s != p));

            baseProduct.LoanPurposes = remainingLoanPurposes;

            BaseProductUpdateRequest baseProductUpdateRequest = baseProduct.ToBaseProductUpdateRequest();

            ApiResult<BaseProductResponse> apiResponse2 = await _baseProductApiMethods.UpdateAsync(request.ModelId, baseProductUpdateRequest);

            if (apiResponse2.HasException)
            {
                return BadRequest();
            }
            return Ok();
        }

        [HttpPost]
        public async Task<IActionResult> AddFeatures(string baseProductId, FeaturesUpdateRequest request)
        {
            ApiResult<BaseProductResponse> getApiResponse = await _baseProductApiMethods.GetAsync(baseProductId);

            if (getApiResponse.HasException)
            {
                return NotFound();
            }

            BaseProductUpdateRequest baseProductUpdateRequest = getApiResponse.Result.ToBaseProductUpdateRequest();

            baseProductUpdateRequest.Features = baseProductUpdateRequest.Features.Concat(request.FeaturesIds.Select(p => new ProductFeatureLite() {
                Id = p,
                Rules = null,
                Status = "Active"
            }));

            ApiResult<BaseProductResponse> updateApiResponse1 = await _baseProductApiMethods.UpdateAsync(baseProductId, baseProductUpdateRequest);

            if (updateApiResponse1.HasException)
            {
                return BadRequest();
            }
            return Ok();
        }

        [HttpDelete]
        public async Task<IActionResult> RemoveFeatures(SubModelsDeleteRequest request)
        {
            ApiResult<BaseProductResponse> apiResponse1 = await _baseProductApiMethods.GetAsync(request.ModelId);

            if (apiResponse1.HasException)
            {
                return BadRequest();
            }

            BaseProductResponse baseProduct = apiResponse1.Result;

            IEnumerable<ProductFeatureLite> remainingFeatures = baseProduct.Features.Where(p => request.SubModelIds.All(s => s != p.Id));

            baseProduct.Features = remainingFeatures;

            BaseProductUpdateRequest baseProductUpdateRequest = baseProduct.ToBaseProductUpdateRequest();

            ApiResult<BaseProductResponse> apiResponse2 = await _baseProductApiMethods.UpdateAsync(request.ModelId, baseProductUpdateRequest);

            if (apiResponse2.HasException)
            {
                return BadRequest();
            }
            return Ok();
        }

        [HttpPost]
        public async Task<JsonResult> RenderCreateBaseProductPartial()
        {
            string viewContent = await _controllerHelper.RenderRazorViewToString(this, "PartialViews/Create/CreateBaseProductPartial", null);

            return new JsonResult(new
            {
                partialView = viewContent
            });
        }

        [HttpPost]
        public async Task<JsonResult> RenderCopyBaseProductPartial()
        {
            string viewContent = await _controllerHelper.RenderRazorViewToString(this, "PartialViews/Create/CreateBaseProductPartial", null);

            return new JsonResult(new
            {
                partialView = viewContent
            });
        }

        [HttpPost]
        public async Task<JsonResult> RenderDataPartial()
        {
            string viewContent = await _controllerHelper.RenderRazorViewToString(this, "PartialViews/DataPartialViews/DataPartial", null);

            return new JsonResult(new
            {
                partialView = viewContent
            });
        }

        [HttpPost]
        public async Task<JsonResult> RenderUpdateBaseProductPartial(BaseProductViewModel viewModel)
        {
            BaseProductUpdateRequest baseProductUpdateRequest = viewModel.ToBaseProductUpdateRequest();

            string viewContent = await _controllerHelper.RenderRazorViewToString(this, "PartialViews/Update/TabsPartial", baseProductUpdateRequest);

            return new JsonResult(new
            {
                partialView = viewContent
            });
        }

        [HttpPost]
        public async Task<JsonResult> RenderUpdateBaseProductIntervalPartial(IntervalUpdateRequest request)
        {
            if (request.Intervals == null)
            {
                request.Intervals = string.Empty;
            }
            string viewContent = await _controllerHelper.RenderRazorViewToString(this, "PartialViews/Update/IntervalsPartial", request);

            return new JsonResult(new
            {
                partialView = viewContent
            });
        }

        public async Task<IActionResult> RenderCreateRulePartial()
        {

            string viewContent = await _controllerHelper.RenderRazorViewToString(this, "PartialViews/Splits/CreateRulePartial", null);

            return new JsonResult(new
            {
                partialView = viewContent
            });
        }

        public async Task<IActionResult> RenderGenerateSplitsPartial()
        {

            string viewContent = await _controllerHelper.RenderRazorViewToString(this, "PartialViews/Splits/GenerateSplitsPartial", null);

            return new JsonResult(new
            {
                partialView = viewContent
            });
        }

        public async Task<IActionResult> RenderRemoveRulesPartial()
        {

            string viewContent = await _controllerHelper.RenderRazorViewToString(this, "PartialViews/Splits/RemoveRulesPartial", null);

            return new JsonResult(new
            {
                partialView = viewContent
            });
        }

        public async Task<IActionResult> RenderAddLoanPurposesPartial()
        {
            string viewContent = await _controllerHelper.RenderRazorViewToString(this, "PartialViews/Details/SideModals/AddLoanPurposesPartial", null);

            return new JsonResult(new
            {
                partialView = viewContent
            });
        }

        public async Task<IActionResult> RenderAddFeaturesPartial()
        {
            string viewContent = await _controllerHelper.RenderRazorViewToString(this, "PartialViews/Details/SideModals/AddFeaturesPartial", null);

            return new JsonResult(new
            {
                partialView = viewContent
            });
        }
    }
}