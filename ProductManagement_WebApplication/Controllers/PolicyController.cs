﻿using Kendo.Mvc.Extensions;
using Microsoft.AspNetCore.Mvc;
using ProductManagement.Api.ApiMethodsInterfaces;
using ProductManagement.Api.Helpers;
using ProductManagement.Api.Requests;
using ProductManagement.Api.Responses.Policy;
using ProductManagement_WebApplication.Requests;
using ProductManagement_WebApplication.ViewModelExtensionMethods;
using ProductManagement_WebApplication.ViewModels;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProductManagement_WebApplication.Controllers
{
    public class PolicyController : Controller
    {
        private readonly IPolicyApiMethods _policyApi;

        public PolicyController(IPolicyApiMethods policyApi)
        {
            _policyApi = policyApi;
        }

        [HttpGet]
        public async Task<IActionResult> GridListPolicies()
        {
            ApiResult<IEnumerable<PolicyResponse>> apiResponse = await _policyApi.ListAsync();

            if (apiResponse.HasException)
            {
                return BadRequest();
            }

            IEnumerable<PolicyViewModel> policies = apiResponse.Result.Select(p => p.ToPolicyViewModel());

            return Ok(policies);
        }

        [HttpGet]
        public IActionResult Policies()
        {
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> ListPolicies(FilterRequest filterRequest)
        {
            ApiResult<IEnumerable<PolicyResponse>> apiResponse = await _policyApi.ListAsync(filterRequest);

            if (apiResponse.HasException)
            {
                return BadRequest();
            }

            IEnumerable<PolicyViewModel> policies = apiResponse.Result.Select(p => p.ToPolicyViewModel());

            return Json(policies);
        }

        [HttpPost]
        public async Task<ActionResult> GetPolicy([FromBody] string id)
        {
            ApiResult<PolicyResponse> apiResponse = await _policyApi.GetAsync(id);

            if (apiResponse.HasException)
            {
                return BadRequest();
            }

            return Json(apiResponse.Result);
        }

        [HttpDelete]
        public async Task<IActionResult> Delete([FromBody] ModelsDeleteRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            ApiResult<bool> apiResponse = await _policyApi.DeleteManyAsync(request.ModelIds);

            if (apiResponse.HasException)
            {
                return BadRequest();
            }
            return Ok();
        }
    }
}
