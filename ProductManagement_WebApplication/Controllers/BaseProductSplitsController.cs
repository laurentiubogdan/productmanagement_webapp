﻿using Microsoft.AspNetCore.Mvc;
using ProductManagement.Api.ApiMethodsInterfaces;
using ProductManagement.Api.Helpers;
using ProductManagement.Api.Requests.BaseProductSplits;
using ProductManagement.Api.Responses.BaseProductSplits;
using System.Threading.Tasks;

namespace ProductManagement_WebApplication.Controllers
{
    public class BaseProductSplitsController : Controller
    {
        private readonly IBaseProductSplitsApiMethods _baseProductSplitsApiMethods;

        public BaseProductSplitsController(IBaseProductSplitsApiMethods baseProductSplitsApiMethods)
        {
            _baseProductSplitsApiMethods = baseProductSplitsApiMethods;
        }

        [HttpPost]
        [RequestFormLimits(ValueCountLimit = 30000000)]
        public async Task<IActionResult> Create(BaseProductSplitsCreateRequest request)
        {
            ApiResult<BaseProductSplitsResponse> apiResponse = await _baseProductSplitsApiMethods.CreateAsync(request);

            if (apiResponse.HasException)
            {
                return BadRequest();
            }

            return Ok(apiResponse.Result);
        }

        [HttpPost]
        [RequestFormLimits(ValueCountLimit = 30000000)]
        public async Task<IActionResult> Update(BaseProductSplitsUpdateRequest request)
        {
            ApiResult<BaseProductSplitsResponse> apiResponse = await _baseProductSplitsApiMethods.UpdateAsync(request);

            if (apiResponse.HasException)
            {
                return BadRequest();
            }

            return NoContent();
        }

        [HttpPost]
        public async Task<IActionResult> Get(string id)
        {
            ApiResult<BaseProductSplitsResponse> apiResponse = await _baseProductSplitsApiMethods.GetAsync(id);

            if (apiResponse.HasException)
            {
                return NoContent();
            }

            return Ok(apiResponse.Result);
        }
    }
}
