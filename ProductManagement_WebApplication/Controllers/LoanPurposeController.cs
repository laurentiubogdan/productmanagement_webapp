﻿using Kendo.Mvc.Extensions;
using Microsoft.AspNetCore.Mvc;
using ProductManagement.Api.ApiMethods;
using ProductManagement.Api.ApiMethodsInterfaces;
using ProductManagement.Api.Helpers;
using ProductManagement.Api.Responses.LoanPurpose;
using ProductManagement_WebApplication.HelpersController.Product;
using ProductManagement_WebApplication.ViewModelExtensionMethods;
using ProductManagement_WebApplication.ViewModels.LoanPurpose;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProductManagement_WebApplication.Controllers
{
    public class LoanPurposeController : Controller
    {
        public readonly ILoanPurposeApiMethods _loanPurposeApiMethods;


        public LoanPurposeController(ILoanPurposeApiMethods loanPurposeApiMethods)
        {
            _loanPurposeApiMethods = loanPurposeApiMethods;
        }

        [HttpGet]
        public async Task<IActionResult> ListLoanPurposes()
        {
            ApiResult<IEnumerable<LoanPurposeResponse>> apiResponse = await _loanPurposeApiMethods.ListAsync();

            if (apiResponse.HasException)
            {
                return BadRequest();
            }

            IEnumerable<LoanPurposeViewModel> loanPurposeViewModel = apiResponse.Result.Select(p => p.ToLoanPurposeViewModel());

            return Ok(loanPurposeViewModel);
        }
    }
}