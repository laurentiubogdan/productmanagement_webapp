﻿using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Mvc;
using ProductManagement.Api.ApiMethodsInterfaces;
using ProductManagement.Api.Helpers;
using ProductManagement.Api.Requests;
using ProductManagement.Api.Requests.Rule;
using ProductManagement.Api.Responses.Rule;
using ProductManagement_WebApplication.ViewModelExtensionMethods;
using ProductManagement_WebApplication.ViewModels.Rules;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProductManagement_WebApplication.Controllers
{
    public class JavaScriptRuleController : Controller
    {
        private readonly IJavaScriptRuleApiMethods _javaScriptRules;

        public JavaScriptRuleController(IJavaScriptRuleApiMethods javaScriptRules)
        {
            _javaScriptRules = javaScriptRules;
        }

        [HttpPost]
        public async Task<IActionResult> GridListAsync([DataSourceRequest] DataSourceRequest request)
        {
            ApiResult<IEnumerable<JavaScriptRuleResponse>> apiResponse = await _javaScriptRules.ListAsync();

            if (apiResponse.HasException)
            {
                return BadRequest();
            }

            IEnumerable<JavaScriptRuleViewModel> javascriptRuleViewModel = apiResponse.Result.Select(rule => rule.ToJavaScriptRuleViewModel());

            DataSourceResult result = javascriptRuleViewModel.ToDataSourceResult(request);

            return Ok(result);
        }

        [HttpPost]
        public async Task<IActionResult> ListByIdsAsync(FilterRequest request)
        {
            ApiResult<IEnumerable<JavaScriptRuleResponse>> apiResponse = await _javaScriptRules.ListQueryAsync(request);

            if (apiResponse.HasException)
            {
                return BadRequest("Filter Request error");
            }
            return Ok(apiResponse.Result);
        }

        [HttpPost]
        public async Task<IActionResult> CreateAsync(JavaScriptRuleCreateRequest request)
        {
            ApiResult<JavaScriptRuleResponse> apiResponse = await _javaScriptRules.CreateAsync(request);

            if (apiResponse.HasException)
            {
                return BadRequest("Create error");
            }

            return Ok(apiResponse.Result);
        }

        [HttpPost]
        public async Task<IActionResult> GetAsync(string id)
        {
            ApiResult<JavaScriptRuleResponse> apiResponse = await _javaScriptRules.GetAsync(id);

            if (apiResponse.HasException)
            {
                return BadRequest("Get error");
            }

            return Ok(apiResponse.Result);
        }
    }
}
