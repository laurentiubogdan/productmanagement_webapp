﻿using Kendo.Mvc.Extensions;
using Microsoft.AspNetCore.Mvc;
using ProductManagement.Api.ApiMethodsInterfaces;
using ProductManagement.Api.Helpers;
using ProductManagement.Api.Requests;
using ProductManagement.Api.Responses.Feature;
using ProductManagement_WebApplication.Requests;
using ProductManagement_WebApplication.ViewModelExtensionMethods;
using ProductManagement_WebApplication.ViewModels;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProductManagement_WebApplication.Controllers
{
    public class FeatureController : Controller
    {
        private readonly IFeatureApiMethods _featureApi;

        public FeatureController(IFeatureApiMethods featureApi)
        {
            _featureApi = featureApi;
        }

        [HttpPost]
        public async Task<IActionResult> GridListFeatures(QueryRequest queryRequest)
        {
            ApiResult<IEnumerable<FeatureResponse>> apiResponse = await _featureApi.ListAsync(queryRequest);

            if (apiResponse.HasException)
            {
                return BadRequest();
            }

            IEnumerable<FeatureViewModel> features = apiResponse.Result.Select(p => p.ToFeatureViewModel());

            return Ok(features);
        }

        [HttpPost]
        public async Task<IActionResult> ListFeatures(QueryRequest queryRequest)
        {
            ApiResult<IEnumerable<FeatureResponse>> apiResponse = await _featureApi.ListAsync(queryRequest);

            if (apiResponse.HasException)
            {
                return BadRequest();
            }

            return Ok(apiResponse.Result);
        }

        [HttpGet]
        public IActionResult Features()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> GetFeature([FromBody] string id)
        {
            ApiResult<FeatureResponse> apiResponse = await _featureApi.GetAsync(id);

            if (apiResponse.HasException)
            {
                return BadRequest();
            }

            return Json(apiResponse.Result);
        }

        [HttpDelete]
        public async Task<IActionResult> Delete([FromBody] ModelsDeleteRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            ApiResult<bool> apiResponse = await _featureApi.DeleteManyAsync(request.ModelIds);

            if (apiResponse.HasException)
            {
                return BadRequest();
            }
            return Ok();
        }
    }
}
