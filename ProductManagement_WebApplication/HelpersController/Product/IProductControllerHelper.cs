﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace ProductManagement_WebApplication.HelpersController.Product
{
    public interface IProductControllerHelper
    {
        Task<string> RenderRazorViewToString(Controller controller, string partialView, object model);
        Task<string> GetViewPageHtml(Controller controller, string viewName, object model);

    }
}
